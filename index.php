<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>.:::FarmaGroup:::.</title>

    <!-- Bootstrap core CSS -->
    <link href="media/css/bootstrap.min.css" rel="stylesheet">
    <link href="media/css/bootstrap-theme.css" rel="stylesheet">
    <link href="media/css/font-awesome.css" rel="stylesheet" type="text/css" >
    <link href="media/css/sb-admin-2.css" rel="stylesheet">
    <link href="media/css/dataTables.bootstrap.css" rel="stylesheet">
    <!-- <link href="../media/css/jquery.dataTables.css" rel="stylesheet" type="text/css" > -->
    <!-- <link href="../media/css/jquery.dataTables_themeroller" rel="stylesheet">
 -->
    <!-- Custom styles for this template -->
    <link href="../media/css/navbar-fixed-top.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Righteous' rel='stylesheet' type='text/css'>
  	<link href='http://fonts.googleapis.com/css?family=Bree+Serif' rel='stylesheet' type='text/css'>
  	<link href='http://fonts.googleapis.com/css?family=Denk+One' rel='stylesheet' type='text/css'>
  	<style type="text/css" media="screen">
  		.font-farma{
  			/*font-family: 'Denk One', sans-serif;*/
  			font-family: 'Bree Serif', serif;
  			/*font-family: 'Righteous', cursive;*/
  			/*color: #3BAFDA;*/
  		}
  		.logo-farma{
  			margin-top: 10px;
  		}
  	</style>
 </head>

  <body >

 <div class="container">
     <div class="row">
     <center>
     	<img class="logo-farma" src="media/images/logo160.jpg" alt="">
     </center>
         <div class="col-md-4 col-md-offset-4">
             <div class="login-panel panel panel-default">
                 <div class="panel-heading text-center">
                     <h3 class="panel-title font-farma">Ingrese sus datos de usuario por favor</h3>
                 </div>
                 <div class="panel-body">
                     <form action="php/auth.php" method="POST" role="form">
                         <fieldset>
                             <div class="input-group form-group">
                                 <span class="input-group-addon"><i class="fa fa-envelope-o fa-fw"></i></span>
                                 <input class="form-control" placeholder="E-mail" name="email" type="email" autofocus>
                             </div>
                             <div class="input-group form-group">
                                 <span class="input-group-addon"><i class="fa fa-key fa-fw"></i></span>
                                 <input class="form-control" placeholder="Password" name="password" type="password" value="">
                             </div>
                             
                             <!-- Change this to a button or input when using this as a form -->
                             <button type="submit" class="font-farma btn btn-success btn-block">Ingresar</button>
                             <?php if (isset($_GET['msg'])) {
                                 $msg= $_GET['msg']; ?>
                                 <br>                               
                                <center>
                                 	<h4><span class="label label-danger">
                                 			<i class="fa fa-exclamation-triangle"></i> <?php echo $msg; ?>
                                 		</span>
                                 	</h4>
                                </center> 
                             <?php } ?>
                         </fieldset>
                     </form>
                 </div>
             </div>
         </div>
     </div>
 </div>

 <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="../media/js/jquery.js"></script>
    <script src="../media/js/bootstrap.min.js"></script>
    <script src="../media/js/jquery.dataTables.js"></script>
    <script src="../media/js/dataTables.bootstrap.js"></script>
    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').dataTable();
    });
    </script>
  </body>
</html>