<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>.:::FarmaGroup:::.</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-theme.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.css">

    <!-- Custom styles for this template -->
    <link href="css/navbar-fixed-top.css" rel="stylesheet">

  </head>

  <body>

    <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php"><i class="fa fa-cog fa-fw"></i> FarmaGroup</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="index.php"><i class="fa fa-search"></i> Buscar</a></li>
            <li><a href="mostrar.php">Mostrar</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <div class="container">
    <div class="row">
      <div class="center-block">
        <div class="col-xs-12 col-md-6 col-lg-12 col-offset-6 centered"> 
          <div class="panel panel-default ">
              <div class="panel-heading">
                  
                  <h4> <i class="fa fa-search"></i> Busqueda de beneficiarios.</h4>     
              </div>
              <div class="panel-body">
              <center>
                  <h4> <i class="fa fa-info-circle"></i> Ingrese la cedula de un titular.</h4>
                  <form class="form-inline" action="buscar.php" method="POST">
                    <div class="form-group">
                        <label class="sr-only" for="tipo_doc">tipo</label>
                        <select class="form-control" id="tipo_doc" name="tipo_doc">
                            <option>V</option>
                            <option>E</option>
                            <option>M</option>
                        </select>
                    </div>
                    <div class="form-group input-group">
                      <input type="text" class="form-control" name="cedula" id="cedula" placeholder="Cedula">
                      <span class="input-group-btn">
                          <button class="btn btn-default" type="submit"><i class="fa fa-search"></i> Buscar
                          </button>
                      </span>
                    </div>
                    
                    <br> <br>
                    <?php if (isset($_GET['msg'])) {
                        $msg= $_GET['msg']; ?>
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong> <?php echo $msg; ?> </strong>
                        </div>
                    <?php } ?>
                  </form>
                  </center>
              </div>
          </div><!--  panel -->
        </div> <!-- column -->
      </div>
    </div> <!-- row -->
      
    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
