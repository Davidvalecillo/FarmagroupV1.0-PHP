<?php 
include("includes/conexion.php");
if (isset($_GET['tipo']) AND isset($_GET['cedula']) ) {
  $tipo = $_GET['tipo'];
  $cedula = $_GET['cedula'];
}
else
{
  $msg = "Ingrese un numero de cedula por favor";
  header("Location: index.php?msg=$msg");
}

 ?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>.:::Data Sidor C.a:::.</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-theme.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.css">

    <!-- Custom styles for this template -->
    <link href="css/navbar-fixed-top.css" rel="stylesheet">

  </head>

  <body>

    <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php"><i class="fa fa-cog fa-fw"></i> DATA SIDOR, C.A</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="index.php"><i class="fa fa-search"></i> Buscar</a></li>
            <li class="active"><a href="mostrar.php">Mostrar</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <div class="container">
    <div class="row">
    
        <?php 
        $sql = mysql_query("SELECT * FROM data_completa WHERE 
                          TIPO_DE_DOCUMENTO_TITULAR = '{$tipo}' AND 
                          CEDULA_DE_IDENTIDAD_TITULAR ='{$cedula}'");
        $filas = mysql_num_rows($sql);
        if ($filas > 0) {
            
            while ($datos = mysql_fetch_assoc($sql)) { ?>
              
                <div class="panel panel-default ">
                    <div class="panel-heading">                      
                        <h4> <i class="fa fa-user"></i> 
                        Beneficiario: <?php echo $datos['NOMBRE_DEL_TITULAR']." ".$datos['APELLIDO_DEL_TITULAR']; ?>
                        <div class="pull-right">Parentesco: <?php echo $datos['PARENTESCO']; ?></div>
                        </h4>     
                    </div>
                    <div class="panel-body">
                      <div class="col-xs-12 col-md-6 col-lg-4">
                        <p><strong>Cedula: </strong><?php echo $datos['TIPO_DE_DOCUMENTO']."-".$datos['CEDULA_DE_IDENTIDAD']; ?></p>
                      </div> 
                      <div class="col-xs-12 col-md-6 col-lg-4">
                        <p><strong>Fecha de Nacimiento: </strong><?php echo $datos['FECHA_NACIMIENTO_TITULAR']; ?></p>
                      </div> 
                      <div class="col-xs-12 col-md-6 col-lg-4">
                        <p><strong>Telefono: </strong><?php echo $datos['TELEFONO']; ?></p>
                      </div>
                      <div class="col-xs-12 col-md-6 col-lg-4">
                        <p><strong>Direccion: </strong><?php echo $datos['DIRECCION']; ?></p>
                      </div>
                      <div class="col-xs-12 col-md-6 col-lg-4">
                        <p><strong>Sexo: </strong><?php echo $datos['SEXO_DEL_TITULAR']; ?></p>
                      </div>
                      <div class="col-xs-12 col-md-6 col-lg-4">
                        <p><strong>Email: </strong><?php echo $datos['EMAIL']; ?></p>
                      </div>
                      <div class="col-xs-12 col-md-6 col-lg-4">
                        <p><strong>Fecha de Nacimiento: </strong><?php echo $datos['FECHA_NACIMIENTO_TITULAR']; ?></p>
                      </div>
                      <?php if ($datos['PARENTESCO']=="TITULAR"): ?>
                          <div class="col-xs-12 col-md-6 col-lg-4">
                            <p><strong>Rif Contratante: </strong><?php echo $datos['RIF_CONTRATANTE']; ?></p>
                          </div>
                          <div class="col-xs-12 col-md-6 col-lg-4">
                            <p><strong>Contratante: </strong><?php echo $datos['NOMBRE_CONTRATANTE']; ?></p>
                          </div>
                          <div class="col-xs-12 col-md-6 col-lg-4">
                            <p><strong>STSASEG: </strong><?php echo $datos['STSASEG']; ?></p>
                          </div>
                      <?php endif ?>
  
                        
                    </div>
                </div><!--  panel -->

          <?php  }
        }
        ?>
          

    </div> <!-- row -->
      
    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
