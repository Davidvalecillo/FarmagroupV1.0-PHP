<?php
  include("../php/restriccion.php");
  include("../includes/conexion.php");
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>FarmaGroup</title>

    <!-- Bootstrap core CSS -->
    <link href="../media/css/bootstrap.min.css" rel="stylesheet">
    <link href="../media/css/bootstrap-theme.css" rel="stylesheet">
    <link href="../media/css/font-awesome.css" rel="stylesheet" type="text/css" >
    <link href="../media/css/sb-admin-2.css" rel="stylesheet">
    <link href="../media/css/dataTables.bootstrap.css" rel="stylesheet">
    <link href="../media/css/select2.css" rel="stylesheet" type="text/css" >
    <link href="../media/css/select2-bootstrap.css" rel="stylesheet" type="text/css" >
    <!-- <link href="../media/css/jquery.dataTables.css" rel="stylesheet" type="text/css" > -->
    <!-- <link href="../media/css/jquery.dataTables_themeroller" rel="stylesheet">
 -->
    <!-- Custom styles for this template -->
    <link href="../media/css/navbar-fixed-top.css" rel="stylesheet">

    <!--<link href='http://fonts.googleapis.com/css?family=Righteous' rel='stylesheet' type='text/css'> -->
    <link href='http://fonts.googleapis.com/css?family=Bree+Serif' rel='stylesheet' type='text/css'> 
    <!--<link href='http://fonts.googleapis.com/css?family=Denk+One' rel='stylesheet' type='text/css'> -->
  <style type="text/css" media="screen">
      .font-farma{
        /*font-family: 'Denk One', sans-serif;*/
        font-family: 'Bree Serif', serif;
        /*font-family: 'Righteous', cursive;*/
        color: #3BAFDA;
      }
    </style> 
</head>

  <body>

  <?php 
      //si es nivel administrador
      if ($_SESSION["nivel"] == 0 ) {
  ?>
    <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand font-farma" href="index.php">
            <img class="logo-farma img-responsive" src="../media/images/logo160.jpg" width="150px" alt="">
          </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="item-nav"><a href="index.php"><i class="fa fa-home"></i> Inicio</a></li>
            <!--<li class="item-nav"><a href="beneficiarios.php"><i class="fa fa-users"></i> Beneficiarios</a></li> -->
            <li class="item-nav"><a href="farmacias.php"><i class="fa fa-hospital-o"></i> Farmacias</a></li>
            <li class="item-nav"><a href="usuarios.php"><i class="fa fa-user"></i> Usuarios</a></li>
            <li class="item-nav"><a href="reportes.php"><i class="fa fa-file-text-o"></i> Reportes</a></li>
            <li class="item-nav"><a href="historial_de_operaciones.php"><i class="fa fa-file-text-o"></i> Historico </a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown item-nav">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-user"></i> <?php echo $_SESSION["nombre"]; ?> <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="perfil.php"><i class="fa fa-male"></i> Perfil</a></li>
                <li><a href="../php/logout.php"><i class="fa fa-sign-out"></i> Salir</a></li>
              </ul>
            </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
<?php    
}
elseif($_SESSION["nivel"] == 1 )
{ 
?>
    <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand font-farma" href="index.php">
            <img class="logo-farma img-responsive" src="../media/images/logo160.jpg" width="150px" alt="">
          </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="item-nav"><a href="index.php"><i class="fa fa-home"></i> Inicio</a></li>
            <!--<li class="item-nav"><a href="beneficiarios.php"><i class="fa fa-users"></i> Beneficiarios</a></li>-->
            <li class="item-nav"><a href="reportes.php"><i class="fa fa-file-text-o"></i> Reportes</a></li>
            <li class="item-nav"><a href="cronicos.php"><i class="fa fa-file-text-o"></i> Cronicos</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown item-nav">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-user"></i> <?php echo $_SESSION["nombre"]; ?> <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="perfil.php"><i class="fa fa-male"></i> Perfil</a></li>
                <li><a href="../php/logout.php"><i class="fa fa-sign-out"></i> Salir</a></li>
              </ul>
            </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
<?php
}
?>
