<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    
    <script src="../media/js/jquery.js"></script>
    <script src="../media/js/bootstrap.min.js"></script>
    <script src="../media/js/jquery.dataTables.js"></script>
    <script src="../media/js/dataTables.bootstrap.js"></script>
    <script src="../media/js/select/select2.js"></script>
    
    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    
    <script>
    $(document).ready(function() {
        $('#dataTables-example').dataTable();
    });
    </script>
  </body>
</html>

