<?php 
include("../includes/header.php");
include("../php/functions.php");
	date_default_timezone_set('America/Caracas');
	if(isset($_POST['fecha'])){ $hoy = $_POST['fecha']; }
	else { $hoy = date('Y-m-d'); }
?>
		<center> <!-- busqueda por una fecha especifica -->
           <p>
             <i class="fa fa-info-circle"></i> Ingrese una fecha especifica:
           </p>
           <form class="form-inline" action="" method="POST">    
               <div class="form-group input-group">
                 <input type="date" class="form-control" name="fecha"  placeholder="" required>
                 <span class="input-group-btn">
                     <button class="btn btn-default" type="submit"><i class="fa fa-search"></i> Buscar
                     </button>
                 </span>
               </div>
               <!-- <button type="submit" class="btn btn-success">Buscar</button> -->
               <br> <br>
               <?php if (isset($_GET['msg'])) {
                   $msg= $_GET['msg']; ?>
                   <div class="alert alert-danger">
                       <button type="button" class="close" data-dismiss="alert">&times;</button>
                       <strong> <?php echo $msg; ?> </strong>
                   </div>
               <?php } ?>
           </form>  
      	</center> 

<div class="container">
	<div class="row">
		<h3> Historial de Operciones del: <?php echo fechaCompleta($hoy); ?> </h3>
		<div class="panel panel-success">
			<div class="panel-heading">
				Datos extras(patologias, medicamentos, frecuencia) registrados
			</div>
			<div class="panel-body">
				<?php 
					$extras = mysql_query("SELECT * FROM aud_datosextra WHERE fecha = '{$hoy}'");
					if(mysql_num_rows($extras) > 0){ ?>
					<table class="table table-hover">
						<thead>
							<tr>
								<th>nro</th>
								<th>Operacion</th>
								<th>Beneficiario</th>
								<th>Tipo Beneficiario</th>
								<th>Fecha</th>
							</tr>
						</thead>
						<tbody>
								<?php 
								$nro = 1;
								while ($ext = mysql_fetch_assoc($extras)) { ?>
									<tr>
										<td> <?php echo $nro; $nro++; ?></td>
										<td> <?php echo $ext['operacion']; ?> </td>
										<td>
											<?php 
												if ($ext['beneficiario_tipo'] == "titular") {
													
													$titular = mysql_query("SELECT nombres, apellidos FROM datos_titular WHERE id = '{$ext['beneficiario_id']}' LIMIT 1 ");
													$name_tit = mysql_fetch_assoc($titular);
													echo $name_tit['nombres']." ".$name_tit['apellidos'];
												}
												elseif ($ext['beneficiario_tipo'] == "familiar") {
													$familiar = mysql_query("SELECT nombres, apellidos FROM datos_familiar WHERE id = '{$ext['beneficiario_id']}' LIMIT 1 ");
													$name_fam = mysql_fetch_assoc($familiar);
													echo $name_fam['nombres']." ".$name_fam['apellidos'];
												}
											?>

										</td>	
										<td> <?php echo $ext['beneficiario_tipo']; ?> </td>
										<td><?php echo $ext['fecha']; ?></td>
									</tr>									
								<?php } ?>
							</tbody>
					</table>


				<?php } else{ ?>
					<i>No se Registraron datos extras en esta fecha.</i>
				<?php } ?>
			</div>
		</div>

	<!-- ///////////////////////// --> 
		<div class="panel panel-success">
			<div class="panel-heading">
				Datos de beneficiarios insertados o modificados.
			</div>
			<div class="panel-body">
				<?php 
					$beneficiarios = mysql_query("SELECT * FROM auditoria_beneficiarios WHERE fecha = '{$hoy}'");
					if(mysql_num_rows($beneficiarios) > 0){ ?>
					<table class="table table-hover">
						<thead>
							<tr>
								<th>nro</th>
								<th>Operacion</th>
								<th>Beneficiario</th>
								<th>Tipo Beneficiario</th>
								<th>Fecha</th>
							</tr>
						</thead>
						<tbody>
								<?php 
								$nro2 = 1;
								while ($bene = mysql_fetch_assoc($beneficiarios)) { ?>
									<tr>
										<td> <?php echo $nro2; $nro2++; ?></td>
										<td> <?php echo $bene['operacion']; ?> </td>
										
											<?php 
												if ($bene['tabla'] == "datos_titular") {
													
													$titular2 = mysql_query("SELECT nombres, apellidos FROM datos_titular WHERE id = '{$bene['id_registro']}' LIMIT 1 ");
													$name_tit2 = mysql_fetch_assoc($titular2);
													echo "<td>".$name_tit2['nombres']." ".$name_tit2['apellidos']."</td>";
													echo "<td> Titular </td>";
												}
												elseif ($bene['tabla'] == "datos_familiar") {
													$familiar2 = mysql_query("SELECT nombres, apellidos FROM datos_familiar WHERE id = '{$bene['id_registro']}' LIMIT 1 ");
													$name_fam2 = mysql_fetch_assoc($familiar2);
													echo "<td>".$name_fam2['nombres']." ".$name_fam2['apellidos']."</td>";
													echo "<td> Familiar </td>";
												}
											?>

										<td><?php echo $bene['fecha']; ?></td>
									</tr>									
								<?php } ?>
							</tbody>
					</table>


				<?php } else{ ?>
					<i>No hay datos para mostrar en esta fecha.</i>
				<?php } ?>
			</div>
		</div>

	</div>
</div>







<?php include("../includes/footer.php"); ?>