<?php 
  include("../includes/header.php");
?>

    <div class="container">
    <div class="row">
        <div class="col-xs-12 col-md-12 col-lg-12"> 
          <div class="panel panel-success ">
              <div class="panel-heading">
                <strong>
                  <i class="fa fa-search"></i> Busqueda rapida de beneficiarios titulares.
                </strong>  
              </div>
              <div class="panel-body">
                <center>
                <p>
                  <i class="fa fa-info-circle"></i> Ingrese la cedula de identidad de un beneficiario titular.
                </p>
                  <form class="form-inline" action="php/buscar.php" method="POST">
                    <div class="form-group">
                        <label class="sr-only" for="tipo_doc">tipo</label>
                        <select class="form-control" id="tipo_doc" name="tipo_doc">
                            <option>V</option>
                            <option>E</option>
                            <option>M</option>
                        </select>
                        <input type="text" class="form-control" name="cedula" id="cedula" placeholder="Cedula">
                    </div>
                    
                    <div class="form-group input-group">
                      <select class="form-control" name="contratante" required>
                          <option value="" selected>Contratante</option>
                          <option value="SIDOR, C.A.">Sidor</option>
                          <option value="MINISTERIO DEL PODER POPULAR PARA LA AGRICULTURA Y TIERRAS">Ministerio Agricultura y Tierras</option>
                      </select>
                      <span class="input-group-btn">
                          <button class="btn btn-success" type="submit"><i class="fa fa-search"></i> Buscar
                          </button>
                      </span>
                    </div>
                    <!-- <button type="submit" class="btn btn-success">Buscar</button> -->
                    <br> <br>
                    <?php if (isset($_GET['msg'])) {
                        $msg= $_GET['msg']; ?>
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong> <?php echo $msg; ?> </strong>
                        </div>
                    <?php } ?>
                  </form>  
                  </center>               
              </div> <!-- panel body -->
          </div><!--  panel -->
        </div> <!-- column -->

    </div> <!-- row -->
      
    </div> <!-- /container -->


    <?php include("../includes/footer.php"); ?>
