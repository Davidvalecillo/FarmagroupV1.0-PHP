<?php 
	include("../includes/header.php");
  include("../php/functions.php");
 ?>

 <div class="container">
 	<div class="row">
    <div class="col-xs-12 col-md-12">
    <center> <a href="reporte_cronico.php" class="btn btn-success btn-lg">Ver Reportes de Cronicos</a> </center>
 		<h1 class="font-farma">Reporte de entregas por farmacia: 

 		</h1><hr>

 		<center> <!-- busqueda por una fecha especifica -->
        
    </center> 
        <div class="col-xs-12 col-md-4">   
        <p>
          <i class="fa fa-info-circle"></i> Ingrese los datos para configurar su reporte.
        </p>
            <form class="" action="" method="POST"> 
              <div class="form-group">
                <label for="">Farmacia:</label>
                <select name="farmacia" id="farmacias" class="form-control" placeholder="" required>
                  
                  <?php $farm = mysql_query("SELECT id, nombre FROM farmacias");
                        while ($fm = mysql_fetch_assoc($farm)) {
                           ?> <option value="<?php echo $fm['id']; ?>"><?php echo $fm['nombre']; ?></option> <?php
                         } 
                  ?>
                </select>
              </div>
              <div class="form-group">
                  <label for="">Fechas:</label>
                  <label class="radio-inline">
                    <input type="radio" name="date" id="date1" value="hoy" onchange="javascript:showContent()" checked> Hoy
                  </label>
                  <label class="radio-inline">
                    <input type="radio" name="date" id="date2" value="periodo" onchange="javascript:showContent()"> Otro Periodo
                  </label>
              </div>
              <div class="form-group" id="content" style="display: none;">
                <label>Desde:</label>
                <input type="date" class="form-control" name="fecha"  id="fecha1">
                <label>Hasta:</label>
                <input type="date" class="form-control" name="fecha2"  id="fecha2">
              </div>   
                <div class="form-group ">
                  <span class="">
                      <button class="btn btn-default" name="buscar" type="submit"><i class="fa fa-search"></i> Buscar
                      </button>
                  </span>
                </div>
                <!-- <button type="submit" class="btn btn-success">Buscar</button> -->
                <br> <br>
                <?php if (isset($_GET['msg'])) {
                    $msg= $_GET['msg']; ?>
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong> <?php echo $msg; ?> </strong>
                    </div>
                <?php } ?>
            </form>  
        </div>
        <div class="col-md-8 col-md-offset-8">
        </div>
        </div>
       	<?php include("consultas_farmacia.php"); ?>
 	</div>
 </div>


 <script type="text/javascript">
 function showContent() {
     element = document.getElementById("content");
     fecha1 = document.getElementById("fecha1");
     fecha2 = document.getElementById("fecha2");
     check = document.getElementById("date2");
     if (check.checked) {
         element.style.display='block';
         fecha1.setAttribute('required','required'); //atributo y valor
         fecha2.setAttribute('required','required'); 
        }
     else {
         element.style.display='none';
         fecha1.removeAttribute('required');
         fecha2.removeAttribute('required');
     }
 }

 </script>

 <?php include("../includes/footer.php"); ?>

 <script type="text/javascript"> 
   $(document).ready(function() { 
      $("#farmacias").select2({
        placeholder: "Seleccione la Farmacia",
        allowClear: true
      });
  });
</script>

<script language="javascript">
  $(document).ready(function() {
  $(".botonExcel").click(function(event) {
  $("#datos_a_enviar").val( $("<div>").append( $("#table-<?php echo $nombre_fm['id'];?>").eq(0).clone()).html());
  $("#FormularioExportacion").submit();
  });
  });

  function objetoAjax(){
   var xmlhttp=false;
   try {
   xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
   } catch (e) {
   try {
   xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
   } catch (E) {
   xmlhttp = false;
   }
   }
   if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
     xmlhttp = new XMLHttpRequest();
     }
     return xmlhttp;
  }
  function eliminarOrden(id){
     //donde se mostrará el resultado de la eliminacion
     divResultado = document.getElementById('delete-ok');
     
     //usaremos un cuadro de confirmacion 
     var eliminar = confirm("Esta seguro que desea eliminar esta orden.?");
     if ( eliminar ) {
     //instanciamos el objetoAjax
     ajax=objetoAjax();
     //uso del medotod GET
     //indicamos el archivo que realizará el proceso de eliminación
     //junto con un valor que representa el id del empleado
     ajax.open("GET", "php/delete-entrega.php?id="+id);
     ajax.onreadystatechange=function() {
     if (ajax.readyState==4) {
     //mostrar resultados en esta capa
     divResultado.innerHTML = ajax.responseText
     }
     }
     //como hacemos uso del metodo GET
     //colocamos null
     ajax.send(null)
     }
  }
</script>