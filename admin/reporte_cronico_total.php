<?php 
	include("../includes/header.php");
  include("../php/functions.php");
 ?>


<div class="container">
	<div class="row">
		<div class="col-xs-12 col-md-12">
			<?php 
				date_default_timezone_set('America/Caracas');
					
						$nro_fact = mysql_query("SELECT nro_factura, created_at FROM factura WHERE 				
									tipo = 'Cronico' 
									GROUP BY nro_factura ");
						?> 
						<h3> Reportes del  </h3> 
						<div id="delete-ok"></div>
						<div class="">
							<diw class="row">
								<div class="panel panel-success">
									<div class="panel-heading">
										<strong>
											<i class="fa fa-medkit"></i> Reportes total de entregas a beneficiarios con patologias cronicas
										</strong>
										<form action="php/ficheroExcel.php" method="POST" target="_blank" id="FormularioExportacion">
											<button type="button" class="botonExcel btn btn-success btn-sm pull-right"><i class="fa fa-file-excel-o"></i> Exportar a Excel</button>
											<input type="hidden" id="datos_a_enviar" name="datos_a_enviar" />
											<input type="hidden" name="id_farma" value="">
											<input type="hidden" id="nombre" value="total_entregas_cronicos"  name="nombre" />
										</form>			
									</div>
									<div class="panel-body">
									<?php 							
										if(mysql_num_rows($nro_fact) > 0)
										{ ?>

										<div class="table-responsive">
											<table class="table  table-hover" id="table-<?php echo $nombre_fm['id']; ?>">						
											<thead>
												<tr>										
													<th>C.I Titular</th>
													<th>Nombre Titular</th>
													<th>Contratante</th>
													<th>C.I Beneficiario</th>
													<th>Nombre Beneficiario</th>
													
													<th>Clave</th>	
													<th>Fecha Creación</th>
													<th>Nro Factura</th>
													<th>Patologia</th>
													<th>Producto Despachado</th>
													<th>Cant</th>
													<th>Precio.U</th>
													<th>PrecioVenta</th>
													<th></th>
												</tr>
											</thead>
											<tbody>							
										<?php
											$total_factura = 0;
											while($facturas_nro = mysql_fetch_assoc($nro_fact))
											{
												
												//definir aqui nueva consulta para contratantes
												
													$factura_group = mysql_query("SELECT * FROM factura WHERE  
														nro_factura = '{$facturas_nro['nro_factura']}'
														AND tipo = 'Cronico'
														 ");
																		
																					
												$count = 0;
												$total_titular = 0;
												while ($factura_end = mysql_fetch_assoc($factura_group))
												{
												$titular_id = $factura_end['titular_id'];
												$familiar_id = $factura_end['familiar_id'];
												$farma_id = $factura_end["farmacia_id"];

												$titular_sql = mysql_query("SELECT * FROM datos_titular WHERE id = '{$titular_id}' LIMIT 1 ");
												$data_titular = mysql_fetch_assoc($titular_sql);

												$flia_sql = mysql_query("SELECT * FROM datos_familiar WHERE id = '{$familiar_id}' LIMIT 1 ");
												$data_flia = mysql_fetch_assoc($flia_sql);

												$farma_sql = mysql_query("SELECT * FROM farmacias WHERE id = '{$farma_id}' LIMIT 1 ");
												$data_farma = mysql_fetch_assoc($farma_sql);
											?>

											<tr class="font-tr" id="<?php echo $factura_end['id']; ?>" data="<?php echo $factura_end['id']; ?>">
												
												<?php if ($count > 0){ ?>
													<td></td> <td></td> 
													<td></td> <td></td> 
													<td></td><td></td><td></td>
													<td></td> <td></td> 
												<?php } else { ?>
												
													<td> <?php echo $data_titular['tipo_doc']."-".$data_titular["cedula"]; ?> </td>
													<td> <?php echo ucwords($data_titular['nombres'])." ".ucwords($data_titular['apellidos']); ?> </td>
													<td> <?php echo $data_titular['nombre_contratante']; ?></td>
													<?php if($familiar_id == 0) {  ?>
														<td><?php echo $data_titular['tipo_doc']."-".$data_titular["cedula"]; ?></td>
														<td><?php echo $data_titular['nombres']." ".$data_titular['apellidos']; ?></td>
													<?php } else { ?>
														<td><?php echo $data_flia['tipo_doc']."-".$data_flia['cedula']; ?></td>
														<td><?php echo $data_flia['nombres']." ".$data_flia['apellidos']; ?></td>
													<?php } ?>
														
														<td><?php echo $factura_end['clave']; ?></td>
														
														<td><?php echo $factura_end['created_at']; ?></td>
														<td class="text-center"><strong> <?php echo $factura_end['nro_factura']; ?></strong></td>
													<td> <?php echo $factura_end['patologia']; ?> </td>
												<?php } ?>
												
												
												<td><?php echo $factura_end['nombre_producto']; ?></td>
												<td class="text-center"><?php echo $factura_end['cantidad']; ?></td>
												<td><?php echo $factura_end['precio_venta']; ?> Bs</td>
												<td><strong><?php echo $factura_end['precio_total']; ?> Bs</strong></td>
												<td> <a href="" class="delete btn btn-danger btn-sm" onclick="eliminarOrden('<?php echo $factura_end['id']; ?>')" id="" title="Eliminar"><i class="fa fa-times"></i></a> </td>
											</tr>
											<?php 
												$total_titular = $total_titular + $factura_end['precio_total'];
												$total_factura = $total_factura + $factura_end['precio_total'];
												$count++; 
											?>
											
										<?php	} ?>

											<tr class="font-tr">
												<td colspan="10" rowspan="" headers=""></td>
													<td colspan="2" class="bg-total-titular">
														<strong>
															Total Factura:
															
														</strong>
													</td>
													<td colspan="1" class="bg-total-titular">
														<strong><?php echo $total_titular; ?> Bs</strong>
													</td>
											</tr>
											
									<?php	} //fin while $facturas_nro	 ?>
											<tr class="font-tr">
												<td colspan="10" rowspan="" headers=""></td>
												<td colspan="2" class="bg-total-farmacia">
													<strong>
														Total Facturado por Farmacia:
														
													</strong>
												</td>
												<td colspan="1" class="bg-total-farmacia">
													<strong><?php echo $total_factura; ?> Bs</strong>
												</td>
											</tr>								
										</tbody>


									</table> 
									</div> <!-- table-responsive fin -->

									<?php	}  //fin IF num_rows($nro_fact)
									else 
									{
										?> No Hay registros guardados en esta fecha. <?php
									}
									?>
									</div>							
								</div>
								<?php 
								
								?>
							</div>
						</div>
		</div>
	</div>
</div>
<?php include("../includes/footer.php"); ?>
<script type="text/javascript">
	$(document).ready(function() {
  		$(".botonExcel").click(function(event) {
  			$("#datos_a_enviar").val( $("<div>").append( $("#table").eq(0).clone()).html());
  			$("#FormularioExportacion").submit();
  		});
  	});
</script>