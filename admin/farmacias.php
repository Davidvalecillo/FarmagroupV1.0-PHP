<?php 
	include("../includes/header.php");
 ?>

 <div class="container">
 	<div class="row">
	 	<center>
	 		<a href="#" data-toggle="modal" data-target="#farmacias" class="btn btn-link">
	 			<h2><span class="label label-success">
	 					<i class="fa fa-hospital-o"></i> Afiliar nueva farmacia al servicio 
	 				</span>
	 			</h2>
	 		</a>

	 	</center>
	 	<br>
	 	<?php if (isset($_GET['msg'])) {
	 	    $msg= $_GET['msg']; ?>
	 	    <div class="alert alert-danger">
	 	        <button type="button" class="close" data-dismiss="alert">&times;</button>
	 	        <strong><?php echo $msg; ?> </strong>
	 	    </div>
	 	<?php } ?>

	<?php 
		$consulta = mysql_query("SELECT * FROM farmacias");
		if (mysql_num_rows($consulta) > 0) 
		{ 
      $count_farm = mysql_num_rows($consulta);
    ?>
    <center> <p class="lead text-primary"> Existen <?php echo $count_farm; ?> Farmacias registradas.</p>  </center>
		
    <?	while ($farmacia = mysql_fetch_assoc($consulta)) { ?>
				<div class="col-xs-12 col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<i class="fa fa-hospital-o"></i> <strong> Farmacia:
							<?php echo $farmacia['nombre']; ?> </strong>
							<div class="pull-right"><strong>RIF: <?php echo $farmacia['rif']; ?> </strong></div>
						</div>
						<div class="panel-body">
							<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
							  	<p><strong>Número Telefonico: </strong><?php echo $farmacia['telefono']; ?></p>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
							  	<p><strong>Email: </strong><?php echo $farmacia['email']; ?></p>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
							  	<p><strong>Ubicacion: </strong><?php echo $farmacia['estado']; ?></p>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
							  	<p><strong>Direccion: </strong><?php echo $farmacia['direccion']; ?></p>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
							  	<p><strong>Encargado: </strong><?php echo $farmacia['nombre_encargado'];?></p>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
							  	<p><strong>Telefono de encargado: </strong><?php echo $farmacia['telefono_encargado']; ?></p>
							</div>

						</div>			
					</div>
				</div>
		<?php	}
			
		}
	?>
 		
 	</div> <!-- div row -->
 </div> <!-- div container -->


 <!--////////////////// Modal Registrar farmacias/////////////////-->
 <!--///////////////////////////////////////////////////////////////////////////////////////-->

    <!-- Modal -->
    <div class="modal fade" id="farmacias" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus"></i> Agregar Farmacias</h4>
          </div>
        <div class="modal-body">
            <form name="familiar" action="php/registrar_farmacia.php" method="POST" class="" >
            
            <div class="row">
            
           <div class="form-group col-xs-12 col-sm-12 col-md-6">
                <label> Nombre Farmacia:</label>
               <input type="text" class="form-control input-sm" name="nombre" required>
            </div>

            <div class="form-group col-xs-12 col-sm-6 col-md-6">
                <label>Rif:</label>
               <input type="text" class="form-control input-sm" name="rif" >
            </div> 

            <div class="form-group col-xs-12 col-sm-6 col-md-6">
                <label> Telefono:</label>
               <input type="text" class="form-control input-sm" name="telefono" required>
            </div>
            <div class="form-group col-xs-12 col-sm-6 col-md-6">
                <label> Email:</label>
               <input type="email input-sm" class="form-control" name="email">
            </div>

            <div class="form-group col-xs-12 col-sm-6 col-md-6">
                <label> Estado:</label>
               <select class="form-control input-sm" id="" name="estado">
                    <option></option>
                    <option>Amazonas</option><option>Anzoátegui</option><option>Apure</option>
                    <option>Aragua</option> <option>Barinas</option><option>Bolívar</option>
                    <option>Carabobo</option><option>Cojedes</option><option>Delta Amacuro</option>
                    <option>Distrito Federal</option> <option>Falcón</option><option>Guárico</option> 
                    <option>Lara</option><option>Mérida</option><option>Miranda</option>
                    <option>Monagas</option><option>Nueva Esparta</option><option>Portuguesa</option>
                    <option>Sucre</option><option>Táchira</option><option>Vargas</option>
                    <option>Yaracuy</option><option>Zulia</option>
                </select> 
            </div>
            <div class="form-group col-xs-12 col-sm-6 col-md-6">
                <label>Ciudad:</label>
               <input type="text" class="form-control input-sm" name="ciudad">
            </div>
            <div class="form-group col-xs-12 col-md-12">
                <label> Direccion:</label>
               <textarea class="form-control input-sm" name="direccion" required title="Ingrese la direccion por favor"> </textarea>
            </div>

            <div class="form-group col-xs-12 col-sm-6 col-md-6">
                <label>Nombre Encargado:</label>
               <input type="text" class="form-control input-sm" name="nombre_encargado">
            </div>

            <div class="form-group col-xs-12 col-sm-6 col-md-6">
                <label> Telefono de Encargado:</label>
               <input type="text" class="form-control input-sm" name="telefono_encargado">
            </div>
            
            
            </div> 
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            <button type="submit" class="btn btn-success">Guardar</button>
            </form>
        </div>
        
        </div>
      </div>
    </div>


 <?php include("../includes/footer.php"); ?>