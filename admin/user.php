<?php 
include("../includes/header.php");
if (isset($_GET['id'])) {
	$id = $_GET['id'];
	$sql = mysql_query("SELECT * FROM usuarios WHERE id = '{$id}' LIMIT 1 ");
	$user = mysql_fetch_assoc($sql);
}
?>

<div class="container">
	<div class="row">
		<div class="col-xs-12 col-md-12">
			<div class="panel panel-success">
				<div class="panel-heading">
					<i class="fa fa-user"></i>
					<strong>Datos del Usuario:</strong>
				</div>

				<div class="panel-body">

					<div class="col-md-4">
						<form action="php/edit_user.php" method="POST" accept-charset="utf-8">
							<input type="hidden" name="id" value="<?php echo $user['id']; ?>">
							<div class="form-group">
								<label for="">Nombre:</label>
								<input type="text" name="nombre" class="form-control" value="<?php echo $user['nombre']; ?>" required >
							</div>
							<div class="form-group">
								<label for="">Cedula:</label>
								<input type="text" name="cedula" class="form-control" value="<?php echo $user['cedula']; ?>" >
							</div>
							<div class="form-group">
								<label for="">Correo Electronico:</label>
								<input type="email" name="email" class="form-control" value="<?php echo $user['email']; ?>" required>
							</div>
							<div class="form-group">
								<label for="">Contraseña:</label>
								<input type="text" name="password" class="form-control" value="<?php echo $user['password']; ?>" >
							</div>
							<div class="form-group">
								<label for="">Farmacia:</label>
								<select name="farmacia" class="form-control" required>
									<?php 
										// mostrar farmacia que ya tiene asignada el usuario
										$fm = mysql_query("SELECT id, nombre FROM farmacias WHERE id = '{$user['farmacia_id']}' LIMIT 1");
										if (mysql_num_rows($fm) == 1) {
											$farma = mysql_fetch_assoc($fm);
											?> <option selected value="<?php echo $farma['id']; ?>"><?php echo $farma['nombre']; ?></option> <?php
										}
										else { ?> <option>No Encontrada</option> <?php }

										// listado de farmacias registradas
										$list_fm = mysql_query("SELECT id, nombre FROM farmacias");
										while ($list = mysql_fetch_assoc($list_fm)) {
											?> <option value="<?php echo $list['id'];?>"> <?php echo $list['nombre'];?> </option> <?php
										}
									?>
								</select>
							</div>		
						
					</div>
					<div class="col-md-8 col-md-offset-8">
					</div>
					<div class="col-md-12">
						<h3>
						<?php if (isset($_GET['msg'])) {
						    if ($_GET['msg'] == 'ok') { ?>
						     	<span class="label label-success">
						     		<i class="fa fa-check"></i>
						     		Los datos fueron actualizados con exito.
						     	</span>	
						<?php } elseif ($_GET['msg'] == 'error') { ?>
								<span class="label label-danger">
						     		<i class="fa fa-exclamation-triangle"></i>
						     		Lo sentimos, ocurrio un error al actualizar.
						     	</span>
						<?php }	
								elseif ($_GET['msg'] == 'delete') { ?>
								<span class="label label-danger">
						     		<i class="fa fa-exclamation-triangle"></i>
						     		Lo sentimos, no se pudo eliminar el usuario.
						     	</span>
						<?php }				    	
						 } ?>
						 </h3>
					</div>
					
				</div>

				<div class="panel-footer">
					<div class="form-group">
						<button type="submit" class="btn btn-success"><i class="fa fa-pencil"></i> Actualizar Datos</button>
						</form>
						<button class="btn btn-danger" data-toggle="modal" data-target="#delete"> <i class="fa fa-trash-o"></i> Eliminar Usuario</button>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>



<!-- /////////////////////////////////////////// Modal  Eliminar titular ///////////////////////////////////////////-->
<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-trash-o"></i>  Eliminar Usuario.</h4>
      </div>
      <div class="modal-body">
        <center>
            <h4>¿Esta seguro que desea eliminar al usuario: <?php echo $user['nombre']; ?> ? </h4>
            <small>Una vez eliminado no podra deshacer la acción.</small>
        </center>
      </div>
      <div class="modal-footer">
          <center>
            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancelar</button>
            <a href="php/delete_user.php?id=<?php echo $user['id']; ?>" class="btn btn-danger btn-sm">Eliminar</a>
          </center>
      </div>
    </div>
  </div>
</div>

<?php include("../includes/footer.php"); ?>