<?php 
	include("../includes/header.php");
 ?>


<div class="container">
	<div class="row">
		<div class="col-xs-12 col-md-6">
			<h2 class="font-farma"> Usuarios para manejo en farmacias. </h2>
		</div>
		<div class="col-xs-12 col-md-6 block-right">
			<a href="#" data-toggle="modal" data-target="#user" class="btn btn-link">
	 			<h2><span class="label label-success">
	 					<i class="fa fa-user-plus"></i> Añadir un nuevo Usuario 
	 				</span>
	 			</h2>
	 		</a>
		</div>	
	</div> <!-- div row -->
	<?php if (isset($_GET['msg'])) {
	    $msg= $_GET['msg']; ?>
	    <div class="alert alert-danger">
	        <button type="button" class="close" data-dismiss="alert">&times;</button>
	        <strong><?php echo $msg; ?> </strong>
	    </div>
	<?php } ?>
	<div class="table-responsive">
		<table class="table table-striped table-hover" id="dataTables-example">
		    <thead>
		        <tr>
		        	<th>N°</th>
		            <th>Nombre</th>
		            <th>Cedula</th>
		            <th>Email</th>
		            <th>Farmacia</th>
		            <th>Accion</th>
		        </tr>
		    </thead>
		    <tbody>
	        <?php 
	            $usuarios = mysql_query("SELECT * FROM usuarios WHERE nivel = '1' ");
	            if (mysql_num_rows($usuarios)>0) {
	            	$nro = 1;      
	            while ($user = mysql_fetch_assoc($usuarios)) {
	        ?>
	            <tr class="odd gradeX">
	            	<td><?php echo $nro; $nro++; ?></td>
	                <td><?php echo $user['nombre']; ?></td>
	                <td><?php echo $user['cedula']; ?></td>
	                <td><?php echo $user['email']; ?></td>
	                <?php $farma = mysql_query("SELECT nombre FROM farmacias WHERE id = '{$user['farmacia_id']}' LIMIT 1"); 
	                	if ($farma) { 
	                		$farmacia = mysql_fetch_assoc($farma);
	                	?>
	                		<td><?php echo $farmacia['nombre']; ?></td>
	                <?php	}
	                else { ?> 
	                		<td>No encontrada</td>
	                <?php }
	                ?>	                	                
	                <td><a href="user.php?id=<?php echo $user['id'];?>" class="btn btn-default btn-xs"><i class="fa fa-user"></i> Ver Usuario</a> </td>
	            </tr>
	            
	         <?php    
	            }
	           }
	        ?>   
	        </tbody>
		</table>

	</div>
	
</div>

<!--////////////////// Modal Registrar user/////////////////-->
 <!--///////////////////////////////////////////////////////////////////////////////////////-->

    <!-- Modal -->
    <div class="modal fade" id="user" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus"></i> Agregar Usuarios</h4>
          </div>
        <div class="modal-body">
            <form name="user" action="php/registrar_users.php" method="POST" class="" >
            
            <div class="row">
            
           <div class="form-group col-xs-12 col-sm-12 col-md-12">
                <label> Nombre:</label>
               <input type="text" class="form-control" name="nombre" required>
            </div>

            <div class="form-group col-xs-12 col-sm-6 col-md-6">
                <label>Cedula:</label>
               <input type="text" class="form-control" name="cedula" pattern="^[0-9]{7,10}$" title="Ingrese de 7 a 10 números">
            </div> 

            <div class="form-group col-xs-12 col-sm-6 col-md-6">
                <label> Email:</label>
               <input type="email" class="form-control" name="email" required>
            </div>

            <div class="form-group col-xs-12 col-sm-6 col-md-6">
                <label>Password:</label>
               <input type="password" class="form-control" name="password" required>
            </div>

            <div class="form-group col-xs-12 col-sm-6 col-md-6">
            <?php $list = mysql_query("SELECT * FROM farmacias");
            ?>
                <label>Farmacia:</label>
               	<select name="farmacia" class="form-control" required>
               		<option value=""></option>
               		<?php 
               			if (mysql_num_rows($list)>0) {
               				while ($li_farma = mysql_fetch_assoc($list)) { ?>
               				<option value="<?php echo $li_farma['id']; ?>"><?php echo $li_farma['nombre']; ?></option>
               			<?php }
               			}
               		 ?>              		
               	</select>
            </div>
            
            </div> 
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            <button type="submit" class="btn btn-success">Guardar</button>
            </form>
        </div>
        
        </div>
      </div>
    </div>


 <?php include("../includes/footer.php"); ?>