
<?php 
	date_default_timezone_set('America/Caracas');
	if(isset($_POST['fecha']) AND isset($_POST['fecha2'])){ 
		$fecha1 = $_POST['fecha']; 
		$fecha2 = $_POST['fecha2'];
		$cont = $_POST['contratante'];
		?> <h3> Reportes desde el <?php echo fechaCompleta($fecha1); ?> hasta el: <?php echo fechaCompleta($fecha2); ?>  </h3> <?php 
	}
	else 
	{ 
		$hoy = date('Y-m-d'); 
		?> <h3> Reportes del <?php echo fechaCompleta($hoy); ?> </h3> <?php 
	}
?>
	
<?php 
	$farmacias = mysql_query("SELECT farmacia_id FROM factura GROUP BY farmacia_id ");

	if(mysql_num_rows($farmacias) > 0)
	{
		while ($id_farmacias = mysql_fetch_assoc($farmacias)) 
		{ 
			$name_farmacias = mysql_query("SELECT * FROM farmacias WHERE id = '{$id_farmacias['farmacia_id']}' ");
			$nombre_fm = mysql_fetch_assoc($name_farmacias);
		?>
			<div class="">
				<diw class="row">
					<div class="panel panel-success">
						<div class="panel-heading">
							<strong>
								<i class="fa fa-medkit"></i> <?php echo $nombre_fm['nombre']; ?>
							</strong>
							
							<form action="php/ficheroExcel.php" method="POST" target="_blank" id="FormularioExportacion<?php echo $nombre_fm['id']; ?>">
								<button type="button" class="botonExcel<?php echo $nombre_fm['id']; ?> mgn btn btn-success btn-sm pull-right"><i class="fa fa-file-excel-o"></i> Exportar a Excel</button>
								<input type="hidden" id="datos_a_enviar" name="datos_a_enviar" />
								<input type="hidden" name="id_farma" value="<?php echo $nombre_fm['id']; ?>">
								<input type="hidden" id="nombre" value="<?php echo $nombre_fm['nombre']; ?>"  name="nombre" />
							</form>	
						</div>
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-hover" id="table-<?php echo $nombre_fm['id']; ?>">
							
								<thead>
									<tr class="">
										<th>C.I Titular</th>
										<th>Nombre Titular</th>
										<th>Contratante</th>
										<th>C.I Beneficiario</th>
										<th>Nombre Beneficiario</th>
										<th>Farmacia</th>
										<th>Clave</th>
										<th>Patologia</th>
										<th>Fecha Creación</th>
										<th>Nro Factura</th>
										<th>Producto Despachado</th>
										<th>Cant</th>
										<th>Precio.U</th>
										<th>PrecioVenta</th>
									</tr>
								</thead>
								<tbody>

						<?php 

							if (isset($_POST['contratante'])) {
								$nro_fact = mysql_query("SELECT nro_factura, created_at, farmacia_id FROM factura 
											WHERE created_at BETWEEN '{$fecha1}' AND '{$fecha2}' 
											AND contratante = '{$cont}'
											AND '{$id_farmacias['farmacia_id']}'
											GROUP BY nro_factura ");
							}
							else
							{
								$nro_fact = mysql_query("SELECT nro_factura, created_at, farmacia_id FROM factura 
											WHERE created_at = '{$hoy}' 
											AND '{$id_farmacias['farmacia_id']}'
											GROUP BY nro_factura ");
							}

							if(mysql_num_rows($nro_fact) > 0)
							{
								
								$total_factura = 0;
								while($facturas_nro = mysql_fetch_assoc($nro_fact))
								{	
																
									if (isset($_POST['contratante'])) {
										$factura_group = mysql_query("SELECT * FROM factura WHERE 
											farmacia_id = '{$id_farmacias['farmacia_id']}' 
											AND nro_factura = '{$facturas_nro['nro_factura']}' 
											AND contratante = '{$cont}' ");
									}
									else
									{
										$factura_group = mysql_query("SELECT * FROM factura WHERE 
											farmacia_id = '{$id_farmacias['farmacia_id']}' 
											AND nro_factura = '{$facturas_nro['nro_factura']}' ");
									}								 
									
									$count = 0;
									$total_titular = 0;
									while ($factura_end = mysql_fetch_assoc($factura_group))
									{
									$titular_id = $factura_end['titular_id'];
									$familiar_id = $factura_end['familiar_id'];
									$farma_id = $factura_end["farmacia_id"];

									$titular_sql = mysql_query("SELECT * FROM datos_titular WHERE id = '{$titular_id}' LIMIT 1 ");
									$data_titular = mysql_fetch_assoc($titular_sql);

									$flia_sql = mysql_query("SELECT * FROM datos_familiar WHERE id = '{$familiar_id}' LIMIT 1 ");
									$data_flia = mysql_fetch_assoc($flia_sql);

									$farma_sql = mysql_query("SELECT * FROM farmacias WHERE id = '{$farma_id}' LIMIT 1 ");
									$data_farma = mysql_fetch_assoc($farma_sql);
								?>

								<tr class="font-tr">
									<?php if ($count > 0){ ?>
										<td></td><td></td><td></td>
										<td></td><td></td><td></td>
										<td></td> <td></td> <td></td>
									<?php } else {  ?>
										
										<td> <?php echo $data_titular['tipo_doc']."-".$data_titular["cedula"]; ?> </td>
										<td> <?php echo $data_titular['nombres']." ".$data_titular['apellidos']; ?> </td>
										<td> <?php echo $data_titular['nombre_contratante']; ?></td>
										<?php if($familiar_id == 0) {  ?>
											<td><?php echo $data_titular['tipo_doc']."-".$data_titular["cedula"]; ?></td>
											<td><?php echo $data_titular['nombres']." ".$data_titular['apellidos']; ?></td>
										<?php } else { ?>
											<td><?php echo $data_flia['tipo_doc']."-".$data_flia['cedula']; ?></td>
											<td><?php echo $data_flia['nombres']." ".$data_flia['apellidos']; ?></td>
										<?php } ?>
										<td><?php echo $data_farma['nombre']; ?></td>
										<td><?php echo $factura_end['clave']; ?></td>
										<td><?php echo $factura_end['patologia']; ?></td>
										<td><?php echo $factura_end['created_at']; ?></td>
									<?php } ?>

									<td class="text-center"><strong> <?php echo $factura_end['nro_factura']; ?></strong></td>
									<td><?php echo $factura_end['nombre_producto']; ?></td>
									<td class="text-center"><?php echo $factura_end['cantidad']; ?></td>
									<td><?php echo $factura_end['precio_venta']; ?> Bs</td>
									<td><?php echo $factura_end['precio_total']; ?> Bs</td>
								</tr>
								<?php 
									$total_titular = $total_titular + $factura_end['precio_total'];
									$total_factura = $total_factura + $factura_end['precio_total'];
									$count++;
								?>
								
							<?php 	}  ?>

								<tr class="font-tr">
									<td colspan="11" rowspan="" headers=""></td>
										<td colspan="2" class="bg-total-titular">
											<strong>
												Total Factura:
												
											</strong>
										</td>
										<td colspan="1" class="bg-total-titular">
											<strong><?php echo $total_titular; ?> Bs</strong>
										</td>
								</tr>
								
						<?php 	} //fin while $facturas_nro	 ?>
								<tr class="font-tr">
									<td colspan="11" rowspan="" headers=""></td>
										<td colspan="2" class="bg-total-farmacia">
											<strong>
												Total Facturado Farmacia:
												
											</strong>
										</td>
										<td colspan="1" class="bg-total-farmacia">
											<strong><?php echo $total_factura; ?> Bs</strong>
										</td>
								</tr>								
							</tbody>


						</table> 
						 <script language="javascript">
						  $(document).ready(function() {
						  $(".botonExcel<?php echo $nombre_fm['id'];?>").click(function(event) {
						  $("#datos_a_enviar").val( $("<div>").append( $("#table-<?php echo $nombre_fm['id'];?>").eq(0).clone()).html());
						  $("#FormularioExportacion<?php echo $nombre_fm['id']; ?>").submit();
						  });
						  });
						</script>
						</div> <!-- table-responsive fin -->

						

						<?php	}  //fin IF num_rows($nro_fact)
						else
						{
							echo "No hay disponibles en esta fecha.";
						}
						?>
						</div>							
					</div>
				</diw>
			</div>


	<?php } //fin while $id_farmacias
		
	}  //fin IF num_rows($farmacias)
	else
	{
		echo "No hay disponibles en esta fecha.";
	}
?>


<script src="../media/js/jquery.js"></script>
<script src="../media/js/bootstrap.min.js"></script>
<script src="../media/js/jquery.dataTables.js"></script>
<script src="../media/js/dataTables.bootstrap.js"></script>