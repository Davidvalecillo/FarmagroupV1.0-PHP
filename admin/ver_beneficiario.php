<?php 
	include("../includes/header.php");
    include("../php/functions.php");
	$id_titular = $_GET['id'];
	$titular = mysql_query("SELECT * FROM datos_titular WHERE id = '{$id_titular}' LIMIT 1 ");
 ?>

<link rel="stylesheet" type="text/css" href="../media/css/jquery-ui-1.7.2.custom.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js"></script>

<script type="text/javascript">
jQuery(function($){
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '&#x3c;Ant',
        nextText: 'Sig&#x3e;',
        currentText: 'Hoy',
        monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
        'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
        'Jul','Ago','Sep','Oct','Nov','Dic'],
        dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
        dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''};
    $.datepicker.setDefaults($.datepicker.regional['es']);
});    
 
$(document).ready(function() {
   $("#datepicker").datepicker();
 });
</script>
 <div class="container">
 	<div class="row">
 	<?php 
 		if ($titular) {
 			$datos = mysql_fetch_assoc($titular);
 	?>
 		<div class="panel panel-primary"> <!-- panel de titular -->
 			<div class="panel-heading">
 				<i class="fa fa-user"></i> 
 				<strong>Beneficiario: <?php echo $datos['nombres']." ".$datos['apellidos']; ?></strong>
 				<div class="pull-right"><strong>Parentesco: Titular</strong></div>
 				
 			</div>
 			<div class="panel-body">
            
 				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
 				  <p><strong>Cedula de Identidad: </strong><?php echo $datos['tipo_doc']."-".$datos['cedula']; ?></p>
 				</div>
 				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
 				  <p><strong>Fecha de Nacimiento: </strong><?php echo $datos['fecha_nacimiento']; ?></p>
 				</div>
 				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
 				  <p><strong>Número Telefonico: </strong><?php echo $datos['telefono']; ?></p>
 				</div>
 				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
 				  <p><strong>Sexo: </strong><?php echo $datos['sexo']; ?></p>
 				</div>
 				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
 				  <p><strong>Email: </strong><?php echo $datos['email']; ?></p>
 				</div>
 				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
 				  <p><strong>Dirección: </strong><?php echo $datos['direccion']; ?></p>
 				</div>
 				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
 				  <p><strong>Rif Contratante: </strong><?php echo $datos['rif_contratante']; ?></p>
 				</div>
 				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
 				  <p><strong>Contratante: </strong><?php echo $datos['nombre_contratante']; ?></p>
 				</div>
 				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
 				  <p><strong>Sfsaseg: </strong><?php echo $datos['sfsaseg']; ?></p>
 				</div>

                <div class="col-xs-12 col-md-12">
                    <p><hr></p>
                    <h4>Información Adicional:</h4>
                    <?php 
                        $selec = mysql_query("SELECT * FROM datos_extras WHERE beneficiario_id = '{$id_titular}' LIMIT 1");
                        if (mysql_num_rows($selec) == 1) { 
                            $dato_extra = mysql_fetch_assoc($selec);
                            ?>
                            <div class="col-xs-12 col-md-12">
                                <p><strong>Fecha de vencimiento:</strong> <?php echo fechaCompleta($dato_extra['fecha_vencimiento']); ?> </p>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <p><strong>Patologias: </strong> <?php echo $dato_extra['patologias']; ?></p>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <p><strong>Medicamenos:</strong> <?php echo $dato_extra['medicamento']; ?></p> 
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <p><strong>Frecuencia del Tratamiento: </strong> <?php echo $dato_extra['frecuencia_tratamiento']; ?></p>
                            </div>
                                                       
                        <?php
                        }
                        else
                        {
                            echo "<li><i>No existe información adicional registrada para este titular.</i></li>";
                        }
                    ?>
                </div>
                
 			</div>
            <div class="panel-footer">
                <center>
                    <button onclick="<?php $id_tit = $datos['id'];?>" class="btn btn-default btn-sm" data-toggle="modal" data-target="#edit_tit" data-togglee="tooltip" data-placement="top" title="Editar Datos"><i class="fa fa-edit"></i></button>
                    <button class="btn btn-default btn-sm" data-toggle="modal" data-target="#delete_tit" data-togglee="tooltip" data-placement="top" title="Eliminar Registro"><i class="fa fa-trash-o"></i></button>
                    <button class="btn btn-default btn-sm" data-toggle="modal" data-target="#extra_tit" data-togglee="tooltip" data-placement="top" title="Agregar Información Adicional"><i class="fa fa-list-ul"></i></button>
                </center>
            </div>
 		</div> <!-- fin panel de titular -->
 		<?php } ?>
 		

<!--////////////////// boton añadir familiar y mensajes de alerta///////////////////////////////////////-->
 <!--///////////////////////////////////////////////////////////////////////////////////////-->
        <?php 
 			$familiar = mysql_query("SELECT * FROM datos_familiar WHERE titular_id = '{$id_titular}'");
 			if ($familiar) {
 				
 				$nro = mysql_num_rows($familiar); ?>
 				<center>
	 				<div class="">
	 					<a href="#" data-toggle="modal" data-target="#familiar" class="btn btn-success btn-lg"><i class="fa fa-user-plus"></i> Añadir familiares beneficiarios</a>
	 				</div>
	 				<h3><span class="label label-default">Este titular posee <?php echo $nro;?> familiares afiliados. </span></h3>
 				</center>

 				<?php if (isset($_GET['msg'])) {
 				    $msg= $_GET['msg']; ?>
 				    <div class="alert alert-danger">
 				        <button type="button" class="close" data-dismiss="alert">&times;</button>
 				        <strong><?php echo $msg; ?> </strong>
 				    </div>
 				<?php } ?>
                <?php if (isset($_GET['success'])) {
                    $msg= $_GET['success']; ?>
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong><?php echo $msg; ?> </strong>
                    </div>
                <?php } ?>

<!--////////////////// paneles de beneficiaro familiar  /////////////////-->
 <!--///////////////////////////////////////////////////////////////////////////////////////-->
 				<?php 
	 				if ($nro>0) 
	 				{    $nf = 1;
	 					while ($dato_familiar = mysql_fetch_assoc($familiar)) 
	 					{ ?>
	 						<div class="panel panel-success"> <!-- panel de titular -->
	 							<div class="panel-heading">
	 								<i class="fa fa-user"></i> 
	 								<strong>Beneficiario: <?php echo $dato_familiar['nombres']." ".$dato_familiar['apellidos']; ?></strong>
	 								<div class="pull-right">
                                        <span class="label label-default font">
                                            <strong>Parentesco: <?php echo $dato_familiar['parentesco']; ?> </strong>
                                        </span>
                                    </div>
	 								
	 							</div>
	 							<div class="panel-body">
                                
	 								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
	 								  <p><strong>Cedula de Identidad: </strong><?php echo $dato_familiar['tipo_doc']."-".$dato_familiar['cedula']; ?></p>
	 								</div>
	 								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
	 								  <p><strong>Fecha de Nacimiento: </strong><?php echo $dato_familiar['fecha_nacimiento']; ?></p>
	 								</div>
	 								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
	 								  <p><strong>Número Telefonico: </strong><?php echo $dato_familiar['telefono']; ?></p>
	 								</div>
	 								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
	 								  <p><strong>Sexo: </strong><?php echo $dato_familiar['sexo']; ?></p>
	 								</div>
	 								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
	 								  <p><strong>Email: </strong><?php echo $dato_familiar['email']; ?></p>
	 								</div>
	 								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
	 								  <p><strong>Dirección: </strong><?php echo $dato_familiar['direccion']; ?></p>
	 								</div>
                                    <div class="col-xs-12 col-md-12">
                                        <p><hr></p>
                                        <h4>Información Adicional:</h4>
                                        <?php 
                                            $selec2 = mysql_query("SELECT * FROM datos_extras WHERE beneficiario_id = '{$dato_familiar['id']}' LIMIT 1");
                                            if (mysql_num_rows($selec2) == 1) { 
                                                $dato_extra_fm = mysql_fetch_assoc($selec2);
                                                ?>
                                                <div class="col-xs-12 col-md-12">
                                                    <p><strong>Fecha de vencimiento:</strong> <?php echo fechaCompleta($dato_extra_fm['fecha_vencimiento']); ?> </p>
                                                </div>
                                                <div class="col-xs-12 col-md-4">
                                                     <p><strong>Patologias: </strong> <?php echo $dato_extra_fm['patologias']; ?></p>
                                                </div>
                                                <div class="col-xs-12 col-md-4">
                                                    <p><strong>Medicamenos:</strong> <?php echo $dato_extra_fm['medicamento']; ?></p>
                                                </div>
                                                <div class="col-xs-12 col-md-4">
                                                    <p><strong>Frecuencia del Tratamiento: </strong> <?php echo $dato_extra_fm['frecuencia_tratamiento']; ?></p>
                                                </div>                
                                            <?php
                                            }
                                            else
                                            {
                                                echo "<li><i>No existe información adicional registrada para este beneficiario.</i></li>";
                                            }
                                        ?>
                                    </div>
	 							</div>
                                <div class="panel-footer">
                                    <center>
                                        <button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#edit_fam<?php echo $nf;?> " data-togglee="tooltip" data-placement="top" title="Editar Datos"><i class="fa fa-edit"></i></button>
                                        <button class="btn btn-default btn-sm" data-toggle="modal" data-target="#delete_fm<?php echo $nf; ?>" data-togglee="tooltip" data-placement="top" title="Eliminar Registro"><i class="fa fa-trash-o"></i></button>
                                        <button class="btn btn-default btn-sm" data-toggle="modal" data-target="#extra_fam<?php echo $nf; ?>" data-togglee="tooltip" data-placement="top" title="Agregar Información Adicional"><i class="fa fa-list-ul"></i></button>
                                    </center>                                   
                                </div>
	 						</div> <!-- fin panel de titular -->

                            <!--////////////////// modal edit familiar  /////////////////-->
                                    <div class="modal fade" id="edit_fam<?php echo $nf;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                      <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel"><i class="fa fa-edit"></i> Editar Datos de Beneficiario</h4>
                                          </div>
                                          <div class="modal-body">
                                          <?php 
                                              $edit_fm = mysql_query("SELECT * FROM  datos_familiar WHERE id = '{$dato_familiar['id']}' LIMIT 1");
                                              if(mysql_num_rows($edit_fm) > 0){
                                                  $dato_flia = mysql_fetch_assoc($edit_fm);
                                              }
                                              else {
                                                  echo "<p class='text-danger'><i class='fa fa-bug'></i> Disculpe, Ha ocurrido un error al encontrar los datos del Beneficiario.</p>";
                                              }
                                          ?>
                                            
                                            <form name="titular" action="php/edit_titular.php" method="POST" class="" >
                                            <input type="hidden" name="id" value="<?php echo $dato_flia['id']; ?>"> 
                                            <input type="hidden" name="id_titular" value="<?php echo $id_titular; ?>"> 
                                            <div class="row"> 
                                            <div class="form-group col-xs-12 col-md-6">
                                                <label> Nombres</label>
                                               <input type="text" class="form-control  input-sm" value="<?php echo $dato_flia['nombres']; ?>" name="nombres" required>
                                            </div>

                                            <div class="form-group col-xs-12 col-md-6">
                                                <label> Apellidos</label>
                                               <input type="text" class="form-control input-sm" value="<?php echo $dato_flia['apellidos']; ?>" name="apellidos" required>
                                            </div>

                                            <div class=" col-xs-12 col-md-6">
                                                <label for="">Cedula de Identidad:</label>
                                                <div class="row">
                                                    <div class="col-xs-3 col-md-4">
                                                        <select class="form-control input-sm" id="tipo" name="tipo">
                                                            <option><?php echo $dato_flia['tipo_doc']; ?></option>
                                                            <option>V</option>
                                                            <option>E</option>
                                                            <option>M</option>
                                                        </select>                                       
                                                    </div>
                                                    <div class="col-xs-9 col-md-8">
                                                        <input type="text" class="form-control input-sm" value="<?php echo $dato_flia['cedula']; ?>" name="cedula" required id="cedula" placeholder="Cedula" pattern="^[0-9]{7,10}$" title="Ingrese de 7 a 10 números">
                                                    </div>
                                                </div>
                                            </div>
                                            

                                            <div class="form-group col-xs-12 col-md-6">
                                                <label> Fecha nacimiento:</label>
                                                <?php 
                                                    $fech = explode("-", $dato_flia['fecha_nacimiento']);
                                                 ?>
                                               <input type="date"  class="form-control input-sm" value="<?php echo $fech[2]."/".$fech[1]."/".$fech[0]; ?>" name="nacimiento">
                                            </div>

                                            <div class="form-group col-xs-12 col-md-12">
                                                <label> Direccion:</label>
                                               <textarea class="form-control input-sm"  name="direccion"><?php echo $dato_flia['direccion']; ?> </textarea>
                                            </div>
                                            <div class="form-group col-xs-12 col-md-6">
                                                <label> Email:</label>
                                               <input type="email" class="form-control input-sm" value="<?php echo $dato_flia['email']; ?>" name="email">
                                            </div>

                                            <div class="form-group col-xs-12 col-md-6">
                                                <label> Telefono:</label>
                                               <input type="text" class="form-control input-sm" value="<?php echo $dato_flia['telefono']; ?>" name="telefono">
                                            </div>

                                            <div class="form-group col-xs-12 col-md-6">
                                                <label>Sexo:</label>
                                                <select class="form-control input-sm" id="" name="sexo">
                                                    <option><?php echo $dato_flia['sexo']; ?></option>
                                                    <option>Femenino</option>
                                                    <option>Masculino</option>
                                                </select>                                       
                                            </div>

                                            <div class="form-group col-xs-12 col-md-6">
                                                <label> Parentesco:</label>
                                               <input type="text" class="form-control input-sm" value="<?php echo $dato_flia['parentesco']; ?>" name="parentesco">
                                            </div>
                                            </div>
                                          </div>
                                          <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                            <button type="submit" name="edit_fam" class="btn btn-primary">Guardar Cambios</button>
                                            </form>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                    <!--///////////////////////////////////////////////////////////// -->
                    <!-- /////////////////////////////////////////// Modal  Eliminar familiar ///////////////////////////////////////////-->
                    <div class="modal fade" id="delete_fm<?php echo $nf; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel"><i class="fa fa-trash-o"></i>  Eliminar Benediciario.</h4>
                          </div>
                          <div class="modal-body">
                            <center>
                                <h4>¿Esta seguro que desea eliminar al beneficiario: <?php echo $dato_familiar['nombres']." ".$dato_familiar['apellidos']; ?> ? </h4>
                            </center>
                          </div>
                          <div class="modal-footer">
                              <center>
                                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancelar</button>
                                <a href="php/delete_familiar.php?id=<?php echo $dato_familiar['id']; ?>&id_titular=<?php echo $id_titular; ?>" class="btn btn-danger btn-sm">Eliminar</a>
                              </center>
                          </div>
                        </div>
                      </div>
                    </div>


                    <!-- /////////////////////////////////////////// Modal  informacion adicional familiar ///////////////////////////////////////////-->
                    <div class="modal fade" id="extra_fam<?php echo $nf; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel"><i class="fa fa-edit"></i> Información Adicional:</h4>
                          </div>
                          <div class="modal-body">
                            <div class="row">
                                <form method="POST" action="php/register_extra.php">
                                    <input type="hidden" name="id_fam" value="<?php echo $dato_familiar['id']; ?>">
                                    <input type="hidden" name="id_titular" value="<?php echo $id_titular; ?>">
                                    <input type="hidden" name="tipo" value="familiar">
                                    <div class="col-xs-12 col-md-6">
                                        <label> Fecha Vencimiento:</label>
                                        <input type="date" class="form-control input-md" required name="fecha_vencimiento">                   
                                    </div>
                                    <div class="form-group col-xs-12 col-md-12">
                                        <label> Patologias:</label>
                                       <input type="text" class="form-control input-md" name="patologia">
                                    </div>
                                    <div class="form-group col-xs-12 col-md-12">
                                        <label> Medicamentos:</label>
                                       <input type="text" class="form-control input-md"  name="medicamentos">
                                    </div>
                                    <div class="form-group col-xs-12 col-md-12">
                                        <label> Frecuencia del tratamiento:</label>
                                       <input type="text" class="form-control input-md" name="frecuencia">
                                    </div>
                                
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="submit" name="familiar" class="btn btn-primary">Guardar</button>
                            </form>
                          </div>
                        </div>
                      </div>
                    </div>



                        <?php $nf++;
	 				 }
	 			} 			
 		}
 		else
 		{ ?>
 			<center>
	 				<h3><span class="label label-danger">Lo sentimos, ha ocurrido un problema y no se encuentran registros. </span></h3>
 			</center>
 		<?php }
 	 ?>
 	</div>
 </div>




 <!--////////////////// Modal Registrar familiar/////////////////-->
 <!--///////////////////////////////////////////////////////////////////////////////////////-->

    <!-- Modal -->
    <div class="modal fade" id="familiar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus"></i> Agregar Familiar Beneficiario</h4>
          </div>
        <div class="modal-body">
            <form name="familiar" action="php/registrar_familiar.php" method="POST" class="" >
            <input type="hidden" name="id" value="<?php echo $id_titular; ?>">
            <div class="row"> 
            <div class="form-group col-xs-12 col-sm-6 col-md-6">
                <label> Nombres</label>
               <input type="text" class="form-control input-sm" name="nombres" required>
            </div>

            <div class="form-group col-xs-12 col-sm-6 col-md-6">
                <label> Apellidos</label>
               <input type="text" class="form-control input-sm" name="apellidos" required>
            </div>

            <div class=" col-xs-12 col-sm-6 col-md-6">
                <label for="">Cedula de Identidad:</label>
                <div class="row">
                    <div class="col-xs-3 col-sm-4 col-md-4">
                        <select class="form-control" input-sm id="tipo" name="tipo">
                            <option>V</option>
                            <option>E</option>
                            <option>M</option>
                        </select>                                       
                    </div>
                    <div class="col-xs-9 col-sm-8 col-md-8">
                        <input type="text" class="form-control input-sm" name="cedula" required id="cedula" placeholder="Cedula" pattern="^[0-9]{7,10}$" title="Ingrese de 7 a 10 números">
                    </div>
                </div>
            </div>
            

            <div class="form-group col-xs-12 col-sm-6 col-md-6">
                <label> Fecha nacimiento:</label>
               <input type="text" id="datepicker" class="form-control input-sm" name="nacimiento">
            </div>

            <div class="form-group col-xs-12 col-md-12">
                <label> Direccion:</label>
               <textarea class="form-control input-sm" name="direccion"> </textarea>
            </div>
            <div class="form-group col-xs-12 col-sm-6 col-md-6">
                <label> Email:</label>
               <input type="email" class="form-control input-sm" name="email">
            </div>

            <div class="form-group col-xs-12 col-sm-6 col-md-6">
                <label> Telefono:</label>
               <input type="text" class="form-control input-sm" name="telefono">
            </div>

            <div class="form-group col-xs-12 col-sm-6 col-md-6">
                <label>Sexo:</label>
                <select class="form-control input-sm" id="" name="sexo">
                    <option></option>
                    <option>Femenino</option>
                    <option>Masculino</option>
                </select>                                       
            </div>

            <div class="form-group col-xs-12 col-sm-6 col-md-6">
                <label>Parentesco:</label>
                <select class="form-control input-sm" name="parentesco" required>
                    <option></option>
                    <option>Conyuge</option>
                    <option>Padre</option>
                    <option>Madre</option>
                    <option>Hijo</option>
                    <option>Hija</option>
                    <option>Abuelo</option>
                    <option>abuela</option>
                </select>                                       
            </div>
            
            </div> 
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            <button type="submit" class="btn btn-success">Guardar</button>
            </form>
        </div>
        
        </div>
      </div>
    </div>

<!-- /////////////////////////////////////////// Modal Edit titular  ///////////////////////////////////////////-->
<div class="modal fade" id="edit_tit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-edit"></i> Editar Datos de Beneficiario</h4>
      </div>
      <div class="modal-body">
        <?php $id_tit; 
            $edit_t = mysql_query("SELECT * FROM  datos_titular WHERE id = '{$id_tit}' LIMIT 1");
            if(mysql_num_rows($edit_t) > 0){
                $editar_t = mysql_fetch_assoc($edit_t);
            }
            else {
                echo "<p class='text-danger'><i class='fa fa-bug'></i> Disculpe, Ha ocurrido un error al encontrar los datos del Beneficiario.</p>";
            }
        ?>

        <form name="titular" action="php/edit_titular.php" method="POST" class="" >
        <input type="hidden" name="id" value="<?php echo $id_tit; ?>">    
        <div class="row"> 
        <div class="form-group col-xs-12 col-md-6">
            <label> Nombres</label>
           <input type="text" class="form-control  input-sm" value="<?php echo $editar_t['nombres']; ?>" name="nombres" required>
        </div>

        <div class="form-group col-xs-12 col-md-6">
            <label> Apellidos</label>
           <input type="text" class="form-control input-sm" value="<?php echo $editar_t['apellidos']; ?>" name="apellidos" required>
        </div>

        <div class=" col-xs-12 col-md-6">
            <label for="">Cedula de Identidad:</label>
            <div class="row">
                <div class="col-xs-3 col-md-4">
                    <select class="form-control input-sm" id="tipo" name="tipo">
                        <option><?php echo $editar_t['tipo_doc']; ?></option>
                        <option>V</option>
                        <option>E</option>
                        <option>M</option>
                    </select>                                       
                </div>
                <div class="col-xs-9 col-md-8">
                    <input type="text" class="form-control input-sm" value="<?php echo $editar_t['cedula']; ?>" name="cedula" required id="cedula" placeholder="Cedula" pattern="^[0-9]{7,10}$" title="Ingrese de 7 a 10 números">
                </div>
            </div>
        </div>
        

        <div class="form-group col-xs-12 col-md-6">
            <label> Fecha nacimiento:</label>
            <?php 
                $fech = explode("-", $editar_t['fecha_nacimiento']);
             ?>
           <input type="date" class="form-control input-sm" value="<?php echo $fech[2]."/".$fech[1]."/".$fech[0]; ?>" name="nacimiento">
        </div>

        <div class="form-group col-xs-12 col-md-12">
            <label> Direccion:</label>
           <textarea class="form-control input-sm"  name="direccion"><?php echo $editar_t['direccion']; ?> </textarea>
        </div>
        <div class="form-group col-xs-12 col-md-6">
            <label> Email:</label>
           <input type="email" class="form-control input-sm" value="<?php echo $editar_t['email']; ?>" name="email">
        </div>

        <div class="form-group col-xs-12 col-md-6">
            <label> Telefono:</label>
           <input type="text" class="form-control input-sm" value="<?php echo $editar_t['telefono']; ?>" name="telefono">
        </div>

        <div class="form-group col-xs-12 col-md-6">
            <label>Sexo:</label>
            <select class="form-control input-sm" id="" name="sexo">
                <option><?php echo $editar_t['sexo']; ?></option>
                <option>Femenino</option>
                <option>Masculino</option>
            </select>                                       
        </div>

        <div class="form-group col-xs-12 col-md-6">
            <label> Rif contratante:</label>
           <input type="text" class="form-control input-sm" name="rif" value="<?php echo $editar_t['rif_contratante']; ?>" >
        </div>

        <div class="form-group col-xs-12 col-md-6">
            <label>Nombre contratante</label>
           <input type="text" class="form-control input-sm" name="contratante" value="<?php echo $editar_t['nombre_contratante']; ?>" >
        </div>
        
        <div class="form-group col-xs-12 col-md-6">
            <label> Sfsaseg:</label>
           <input type="text" class="form-control input-sm" name="sfsaseg" value="<?php echo $editar_t['sfsaseg']; ?>" >
        </div>
        </div> 

      </div> <!-- fin modal-body -->
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancelar</button>
        <button type="submit" name="edit_tit" class="btn btn-primary btn-sm">Guardar Cambios</button>
        </form>
      </div>
            
    </div>
  </div>
</div>



<!-- /////////////////////////////////////////// Modal  Eliminar titular ///////////////////////////////////////////-->
<div class="modal fade" id="delete_tit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-trash-o"></i>  Eliminar Benediciario.</h4>
      </div>
      <div class="modal-body">
        <center>
            <h4>¿Esta seguro que desea eliminar al titular: <?php echo $datos['nombres']." ".$datos['apellidos']; ?> ? </h4>
            <small>Si lo hace tambien se eliminaran sus beneficiarios afiliados.</small>
        </center>
      </div>
      <div class="modal-footer">
          <center>
            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancelar</button>
            <a href="php/delete_titular.php?id=<?php echo $datos['id']; ?>" class="btn btn-danger btn-sm">Eliminar</a>
          </center>
      </div>
    </div>
  </div>
</div>


<!-- /////////////////////////////////////////// Modal  informacion adicional titular ///////////////////////////////////////////-->
<div class="modal fade" id="extra_tit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-edit"></i> Información Adicional</h4>
      </div>
      <div class="modal-body">
        <div class="row">
            <form method="POST" action="php/register_extra.php">
                <input type="hidden" name="id_tit" value="<?php echo $id_titular; ?>">
                <input type="hidden" name="tipo" value="titular">
                <div class="col-xs-12 col-md-6">
                    <label> Fecha Vencimiento:</label>
                    <input type="date" class="form-control input-md" required name="fecha_vencimiento">                   
                </div>
                <div class="form-group col-xs-12 col-md-12">
                    <label> Patologias:</label>
                   <input type="text" class="form-control input-md" name="patologia">
                </div>
                <div class="form-group col-xs-12 col-md-12">
                    <label> Medicamentos:</label>
                   <input type="text" class="form-control input-md"  name="medicamentos">
                </div>
                <div class="form-group col-xs-12 col-md-12">
                    <label> Frecuencia del tratamiento:</label>
                   <input type="text" class="form-control input-md" name="frecuencia">
                </div>
            
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="submit" name="titular" class="btn btn-primary">Guardar</button>
        </form>
      </div>
    </div>
  </div>
</div>


 

 <?php include("../includes/footer.php"); ?>

 <script type="text/javascript">
    $(function () {
        $('[data-togglee="tooltip"]').tooltip()
    });
 </script>
