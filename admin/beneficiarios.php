<?php 
	include("../includes/header.php");
 ?>

 <div class="container">
 	<div class="row">
 		<div class="col-xs-12 col-md-12 col-lg-12">
 			<div class="panel panel-success">
            <div class="panel-heading">
            <h5> 
                <i class="fa fa-users"></i> Listado de beneficiarios titulares
                <div class="pull-right">
                	<a href="#" data-toggle="modal" data-target="#titular" class="btn btn-success margin "><i class="fa fa-plus"></i> Nuevo titular</a>
                </div>
            </h5>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
            <?php if (isset($_GET['msg'])) {
                $msg= $_GET['msg']; ?>
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong><?php echo $msg; ?> </strong>
                </div>
            <?php } ?>
                <div class="table-responsive">
                    <table class="table table-striped table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Cedula</th>
                                <th>Telefono</th>
                                <th>Direccion</th>
                                <th>Accion</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php 
                            $titulares = mysql_query("SELECT * FROM datos_titular");
                            while ($titular = mysql_fetch_assoc($titulares)) {
                        ?>

                            <tr class="odd gradeX">
                                <td><?php echo $titular['nombres']." ".$titular['apellidos']; ?></td>
                                <td><?php echo $titular['tipo_doc']."-".$titular['cedula']; ?></td>
                                <td><?php echo $titular['telefono']; ?></td>
                                <td><?php echo $titular['direccion']; ?></td>
                                <td><a href="ver_beneficiario.php?id=<?php echo $titular['id'];?>" class="btn btn-default btn-xs">Ver</a> </td>
                            </tr>
                            
                         <?php    
                            }
                        ?>   
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
                
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
 		</div>
 	</div>
 </div>

 <!--////////////////// Modal Registrar Titular/////////////////-->
 <!--///////////////////////////////////////////////////////////////////////////////////////-->

    <!-- Modal -->
    <div class="modal fade" id="titular" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus"></i> Agregar nuevo Beneficiario Titular</h4>
          </div>
        <div class="modal-body">
            <form name="titular" action="php/registrar_titular.php" method="POST" class="" >
            
            <div class="row"> 
            <div class="form-group col-xs-12 col-md-6">
                <label> Nombres</label>
               <input type="text" class="form-control" name="nombres" required>
            </div>

            <div class="form-group col-xs-12 col-md-6">
                <label> Apellidos</label>
               <input type="text" class="form-control" name="apellidos" required>
            </div>

            <div class=" col-xs-12 col-md-6">
                <label for="">Cedula de Identidad:</label>
                <div class="row">
                    <div class="col-xs-3 col-md-4">
                        <select class="form-control" id="tipo" name="tipo">
                            <option>V</option>
                            <option>E</option>
                            <option>M</option>
                        </select>                                       
                    </div>
                    <div class="col-xs-9 col-md-8">
                        <input type="text" class="form-control" name="cedula" required id="cedula" placeholder="Cedula" pattern="^[0-9]{7,10}$" title="Ingrese de 7 a 10 números">
                    </div>
                </div>
            </div>
            

            <div class="form-group col-xs-12 col-md-6">
                <label> Fecha nacimiento:</label>
               <input type="date" class="form-control" name="nacimiento">
            </div>

            <div class="form-group col-xs-12 col-md-12">
                <label> Direccion:</label>
               <textarea class="form-control" name="direccion"> </textarea>
            </div>
            <div class="form-group col-xs-12 col-md-6">
                <label> Email:</label>
               <input type="email" class="form-control" name="email">
            </div>

            <div class="form-group col-xs-12 col-md-6">
                <label> Telefono:</label>
               <input type="text" class="form-control" name="telefono">
            </div>

            <div class="form-group col-xs-12 col-md-6">
                <label>Sexo:</label>
                <select class="form-control" id="" name="sexo">
                    <option></option>
                    <option>Femenino</option>
                    <option>Masculino</option>
                </select>                                       
            </div>

            <div class="form-group col-xs-12 col-md-6">
                <label> Rif contratante:</label>
               <input type="text" class="form-control" name="rif" value="G-20010626" readonly>
            </div>

            <div class="form-group col-xs-12 col-md-6">
                <label>Nombre contratante</label>
               <input type="text" class="form-control" name="contratante" value="Sidor, C.A." readonly>
            </div>
            
            <div class="form-group col-xs-12 col-md-6">
                <label> Sfsaseg:</label>
               <input type="text" class="form-control" name="sfsaseg" value="I" readonly>
            </div>
            </div> 
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default btn-lg " data-dismiss="modal">Cancelar</button>
            <button type="submit" class="btn btn-success btn-lg ">Guardar</button>
            </form>
        </div>
        
        </div>
      </div>
    </div>


 <?php include("../includes/footer.php"); ?>