<?php 
function fecha_total($fecha_hoy)
{
  //$fecha_hoy = date('Y-m-d');
  $dias = array('', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo');
  $dia = $dias[date('N', strtotime($fecha_hoy))];
  
  $fecha_separada= explode("-", $fecha_hoy);

  $anio= $fecha_separada[0];
   
  switch ($fecha_separada[1]) {
    
    case "01":
        $mes="Enero";
      break;
    case "02":
        $mes="Febrero";
      break;
    case "03":
        $mes="Marzo";
      break;
    case "04":
        $mes="Abril";
      break;
    case "05":
        $mes="Mayo";
      break;
    case "06":
        $mes="Junio";
      break;
    case "07":
        $mes="Julio";
      break;
    case "08":
        $mes="Agosto";
      break;
    case "09":
        $mes="Septiembre";
      break;
    case "10":
        $mes="Octubre";
      break;
    case "11":
        $mes="Noviembre";
      break;
    case "12":
        $mes="Diciembre";
      break;
    }
    return $dia." ".$fecha_separada[2]." de ".$mes." del ".$anio;
}



function fechaCompleta($fecha){
  $fecha_separada= explode("-", $fecha);

  $anio= $fecha_separada[0];
  $dia= $fecha_separada[2];
  
  switch ($fecha_separada[1]) {
    
    case "01":
        $mes="Enero";
      break;
    case "02":
        $mes="Febrero";
      break;
    case "03":
        $mes="Marzo";
      break;
    case "04":
        $mes="Abril";
      break;
    case "05":
        $mes="Mayo";
      break;
    case "06":
        $mes="Junio";
      break;
    case "07":
        $mes="Julio";
      break;
    case "08":
        $mes="Agosto";
      break;
    case "09":
        $mes="Septiembre";
      break;
    case "10":
        $mes="Octubre";
      break;
    case "11":
        $mes="Noviembre";
      break;
    case "12":
        $mes="Diciembre";
      break;

    default: $mes = "fail";
      break;
  }
    return $dia." de ". $mes." de ". $anio;
}

function ContarDias($fecha){
  //defino fecha 1 
  $fech = explode(' ', $fecha);
  $fecha_date = $fech[0];

  $fecha_final = explode('-', $fecha_date);
  $ano1 = $fecha_final[0]; 
  $mes1 = $fecha_final[1]; 
  $dia1 = $fecha_final[2]; 

  //defino fecha actual 
  $ano2 = date('Y'); 
  $mes2 = date('m'); 
  $dia2 = date('d'); 

  //calculo timestam de las dos fechas 
  $timestamp1 = mktime(0,0,0,$mes1,$dia1,$ano1); 
  $timestamp2 = mktime(4,12,0,$mes2,$dia2,$ano2); 

  //resto a una fecha la otra 
  $segundos_diferencia = $timestamp1 - $timestamp2; 
  //echo $segundos_diferencia; 

  //convierto segundos en días 
  $dias_diferencia = $segundos_diferencia / (60 * 60 * 24); 

  //obtengo el valor absoulto de los días (quito el posible signo negativo) 
  $dias_diferencia = abs($dias_diferencia); 

  //quito los decimales a los días de diferencia 
  $dias_diferencia = floor($dias_diferencia); 

  echo $dias_diferencia. " dias"; 
}

?>
