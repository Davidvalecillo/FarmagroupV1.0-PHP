<?php 
include("../includes/conexion.php");

$email = $_POST['email'];
$pass = sha1($_POST['password']);

if (!empty($email) && !empty($pass)) { //verificando que los campos no esten vacios
	
	$verifica = mysql_query("SELECT * FROM usuarios WHERE email = '{$email}' AND password = '{$pass}' LIMIT 1");
	if (mysql_num_rows($verifica)==1) {
		//usuario autorizado con exito
		$user = mysql_fetch_assoc($verifica);
		//asigno un nombre a la sesión para poder guardar diferentes datos
		session_name("userFarma");
		// inicio la sesión 
		session_start(); 
		//defino la fecha y hora de inicio de sesión en formato aaaa-mm-dd hh:mm:ss 
		$_SESSION["ultimoAcceso"] = date("Y-n-j H:i:s"); 
		$_SESSION["autentificado"] = "SI";
		//se guardan datos del usuario en la session
		$_SESSION['id'] = $user['id'];
		$_SESSION["nombre"] = $user['nombre'];
		$_SESSION['cedula'] = $user['cedula'];
		$_SESSION["nivel"] = $user['nivel'];
		$_SESSION["email"] = $user['email'];
		$_SESSION["farmacia"] = $user['farmacia_id'];
		
		//redireccion segun el nivel del usuario
		if ($_SESSION["nivel"]== 1) {
			header("Location: ../users/index.php");
			exit();
		}
		elseif ($_SESSION["nivel"]== 0) {
			header("Location: ../admin/index.php");
			exit();
		}
		else
		{
			//redirecciono al login con un mensaje
			$msg = "Lo sentimos, no se reconoce su nivel de usuario.";
			header("Location: ../index.php?msg=$msg");
			exit();
		}
	}
	else
	{
		//el usuario no fue autorizado
		$msg = "Su email y/o contraseña son incorrectos.";
		header("Location: ../index.php?msg=$msg");
		exit();

	}
}
else
{
	//los campos se encontraron vacios
	$msg = "Debe llenar todos los campos.";
	header("Location: ../index.php?msg=$msg");
	exit();
}

//liberamos los resultados de la consulta
mysql_free_result($verifica); 
?>