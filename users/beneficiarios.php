<?php 
	include("../includes/header.php");
 ?>

 <div class="container">
 	<div class="row">
 		<div class="col-xs-12 col-md-12 col-lg-12">
 			<div class="panel panel-success">
            <div class="panel-heading">
            <h5> <strong>
                <i class="fa fa-users"></i>  Listado de beneficiarios titulares
                </strong>
                <div class="pull-right">
                	
                </div>
            </h5>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
            <?php if (isset($_GET['msg'])) {
                $msg= $_GET['msg']; ?>
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong><?php echo $msg; ?> </strong>
                </div>
            <?php } ?>
                <div class="table-responsive">
                    <table class="table table-striped table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Cedula</th>
                                <th>Telefono</th>
                                <th>Direccion</th>
                                <th>Accion</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php 
                            $titulares = mysql_query("SELECT * FROM datos_titular");
                            while ($titular = mysql_fetch_assoc($titulares)) {
                        ?>

                            <tr class="odd gradeX">
                                <td><?php echo $titular['nombres']." ".$titular['apellidos']; ?></td>
                                <td><?php echo $titular['tipo_doc']."-".$titular['cedula']; ?></td>
                                <td><?php echo $titular['telefono']; ?></td>
                                <td><?php echo $titular['direccion']; ?></td>
                                <td><a href="ver_beneficiario.php?id=<?php echo $titular['id'];?>" class="btn btn-default btn-xs">Ver</a> </td>
                            </tr>
                            
                         <?php    
                            }
                        ?>   
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
                
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
 		</div>
 	</div>
 </div>

 <?php include("../includes/footer.php"); ?>