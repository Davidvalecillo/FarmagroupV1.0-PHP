<?php 
	include("../includes/header.php");
  include("../php/functions.php");
 ?>

 <div class="container">
 	<div class="row">
    <div class="col-xs-12 col-md-12">
 		<h1 class="font-farma">Reporte diario de entregas: 

 		</h1><hr>

 		<center> <!-- busqueda por una fecha especifica -->
            <p>
              <i class="fa fa-info-circle"></i> Ingrese una fecha especifica para ver el reporte.
            </p>
            <form class="form-inline" action="" method="POST"> 
                <div class="form-group">
                  <label>Desde:</label>
                  <input type="date" class="form-control" name="fecha" required>
                  <label>Hasta:</label>
                  <input type="date" class="form-control" name="fecha2" required>
                </div>   
                <div class="form-group">
                  
                      <button class="btn btn-default" name="buscar" type="submit"><i class="fa fa-search"></i> Buscar
                      </button>
                  
                </div>
                <!-- <button type="submit" class="btn btn-success">Buscar</button> -->
                <br> <br>
                <?php if (isset($_GET['msg'])) {
                    $msg= $_GET['msg']; ?>
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong> <?php echo $msg; ?> </strong>
                    </div>
                <?php } ?>
            </form>  
       	</center> 
      </div>
       	<?php include("consultas_farmacia.php"); ?>
 	</div>
 </div>
<?php include("../includes/footer.php"); ?>


<script type="text/javascript">
  $(document).ready(function() {
    $(".botonExcel").click(function(event) {
      $("#datos_a_enviar").val( $("<div>").append( $("#table").eq(0).clone()).html());
      $("#FormularioExportacion").submit();
    });
  });

 function objetoAjax(){
   var xmlhttp=false;
   try {
   xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
   } catch (e) {
   try {
   xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
   } catch (E) {
   xmlhttp = false;
   }
   }
   if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
     xmlhttp = new XMLHttpRequest();
     }
     return xmlhttp;
  }
  function eliminarOrden(id){
     //donde se mostrará el resultado de la eliminacion
     divResultado = document.getElementById('delete-ok');
     
     //usaremos un cuadro de confirmacion 
     var eliminar = confirm("Esta seguro que desea eliminar esta orden.?");
     if ( eliminar ) {
     //instanciamos el objetoAjax
     ajax=objetoAjax();
     //uso del medotod GET
     //indicamos el archivo que realizará el proceso de eliminación
     //junto con un valor que representa el id del empleado
     ajax.open("GET", "php/delete-entrega.php?id="+id);
     ajax.onreadystatechange=function() {
     if (ajax.readyState==4) {
     //mostrar resultados en esta capa
     divResultado.innerHTML = ajax.responseText
     }
     }
     //como hacemos uso del metodo GET
     //colocamos null
     ajax.send(null)
     }
  } 

</script>
 