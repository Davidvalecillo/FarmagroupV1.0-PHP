<?php 
	include("../includes/header.php"); ?>
<?php 
//rescatando lod id de titular y familiar
$titular_id = $_GET['titular'];
$familiar_id = $_GET['familiar'];

//consulta a datos de titulares y familiares
$sql_titular = mysql_query("SELECT * FROM datos_titular WHERE id = '{$titular_id}' LIMIT 1");
$datos_titular = mysql_fetch_assoc($sql_titular);

$sql_familiar = mysql_query("SELECT * FROM datos_familiar WHERE id = '{$familiar_id}' AND titular_id = '{$titular_id}' LIMIT 1");
$datos_familiar = mysql_fetch_assoc($sql_familiar);

//consulta del nombre de la farmacia
$farma_id = $_SESSION["farmacia"];
$farma = mysql_query("SELECT * FROM farmacias WHERE id = '{$farma_id}' LIMIT 1");
$name_farma = mysql_fetch_assoc($farma);
?>

<div class="container">
	<div class="row">
		<div class="col-xs-12 col-md-12">
			<div class="panel panel-success">
				<div class="panel-heading">
					<i class="fa fa-cart-plus fa-lg"></i>  Servicio de entrega directa de medicinas contrante Sidor, C.A
				</div>
				<div class="panel-body">
					<div class="table-responsive">
					<table class="table table-striped table-hover">
					
						<thead>
							<tr>
								<th>C.I Titular</th>
								<th>Nombre Titular</th>
								<th>Contratante</th>
								<th>C.I Beneficiario</th>
								<th>Nombre Beneficiario</th>
								<th>Farmacia</th>
								<th>Fecha</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><?php echo $datos_titular['tipo_doc']."-".$datos_titular['cedula']; ?></td>
								<td><?php echo $datos_titular['nombres']." ".$datos_titular['apellidos']; ?></td>
								<td><?php echo $datos_titular['nombre_contratante']; ?></td>
								<?php if($familiar_id == 0) { ?>
								<td><?php echo $datos_titular['tipo_doc']."-".$datos_titular['cedula']; ?></td>
								<td><?php echo $datos_titular['nombres']." ".$datos_titular['apellidos']; ?></td>
								<?php } else { ?>
								<td><?php echo $datos_familiar['tipo_doc']."-".$datos_familiar['cedula']; ?></td>
								<td><?php echo $datos_familiar['nombres']." ".$datos_familiar['apellidos']; ?></td>
								<?php } ?>
								<td><?php echo $name_farma['nombre']; ?></td>
								
								<td><?php echo date('d-m-Y / h:i. A') ?></td>
							</tr>
						</tbody>
					</table> 
					</div> <!-- table-responsive fin -->	
				</div>
			


    <div class="row">
    	<div class="col-xs-12 col-md-12 table-responsive">
			<table class="table table-bordered table-hover table-sortable">
				<thead>
					<tr>
						<th class="text-center">
							Nro Factura
						</th>
						<th class="text-center">
							Nombre producto despachado
						</th>
						<th class="text-center">
							Cantidad
						</th>
    					<th class="text-center">
							Precio venta
						</th>
        				<th class="text-center" style="border-top: 1px solid #ffffff; border-right: 1px solid #ffffff;">
						</th>
					</tr>
				</thead>
				<tbody id="contenedor">
					<form action="php/registrar_facturas.php" method="POST" accept-charset="utf-8">
						<input type="hidden" name="titular" value="<?php echo $titular_id; ?>">
						<input type="hidden" name="familiar" value="<?php echo $familiar_id; ?>">
						<input type="hidden" name="farmacia" value="<?php echo $farma_id; ?>">

    				<tr>
						<td>
						    <input type="text" name="nro_factura[]"  placeholder="Nro factura" class="form-control"/>
						</td>
						<td>
						    <input type="text" name="name_producto[]" placeholder="Nombre Producto" class="form-control"/>
						</td>
						<td>								
							<input type="number" name="cantidad[]" id="cant" class="form-control text-center" value="1"/>							
						</td>
    					<td>
						    <input type="text" name="precio[]" placeholder="Precio" class="form-control"/>
						</td>
                        <td>
                            <button name="del0" class="eliminar btn btn-danger glyphicon glyphicon-remove "></button>
                        </td>
					</tr>
					
				</tbody>
			</table>
		</div>
	<div class="padding pull-right">
		<button type="submit" class="btn btn-success"><i class="fa fa-plus"></i> Registar</button>
		<a id="add" class="btn btn-default"><i class="fa fa-plus"></i> Añadir Medicamento</a>
		</form>
	</div>
	
</div>
</div>
		</div>
	</div>
</div>


<?php include("../includes/footer.php"); ?>
<script text="text/javascript">
$(document).ready(function() {

    var MaxInputs       = 10; //Número Maximo de Campos
    var contenedor       = $("#contenedor"); //ID del contenedor
    var AddButton       = $("#add"); //ID del Botón Agregar

    //var x = número de campos existentes en el contenedor
    var x = $("#contenedor tbody").length + 1 - 2;
    var FieldCount = x-1; //para el seguimiento de los campos

    $(AddButton).click(function (e) {
        if(x <= MaxInputs) //max input box allowed
        {
            FieldCount++;
            //agregar campo
            $(contenedor).append('<tr><td><input type="text" name="nro_factura[]"  placeholder="Nro factura" class="form-control"/></td><td><input type="text" name="name_producto[]" placeholder="Nombre Producto" class="form-control"/></td><td><input type="number" name="cantidad[]" id="cant" class="form-control text-center" value="1"/></td><td><input type="text" name="precio[]" placeholder="Precio" class="form-control"/></td> <td><button name="del0" class="eliminar btn btn-danger glyphicon glyphicon-remove row-remove"></button></td></tr>');
            x++; //text box increment
        }
        return false;
    });

    $("tbody").on("click",".eliminar", function(e){ //click en eliminar campo
        if( x > 1 ) {
            $(this).parent('tr').remove(); //eliminar el campo
            x--;
        }
        return false;
    });
});
	
</script>