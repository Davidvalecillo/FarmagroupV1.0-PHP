<?php 
	include("../includes/header.php"); 

date_default_timezone_set('America/Caracas');
//rescatando los id de titular y familiar
$titular_id = $_GET['titular'];
$familiar_id = $_GET['familiar'];
if($familiar_id == 0){
	$beneficiario = $titular_id;
}
else{
	$beneficiario = $familiar_id;
}

//consulta a datos de titulares y familiares
$sql_titular = mysql_query("SELECT * FROM datos_titular WHERE id = '{$titular_id}' LIMIT 1");
$datos_titular = mysql_fetch_assoc($sql_titular);

$sql_familiar = mysql_query("SELECT * FROM datos_familiar WHERE id = '{$familiar_id}' AND titular_id = '{$titular_id}' LIMIT 1");
$datos_familiar = mysql_fetch_assoc($sql_familiar);

//consulta del nombre de la farmacia
$farma_id = $_SESSION["farmacia"];
$farma = mysql_query("SELECT * FROM farmacias WHERE id = '{$farma_id}' LIMIT 1");
$name_farma = mysql_fetch_assoc($farma);
?>

<div class="container">
	<div class="row">
		<div class="col-xs-12 col-md-12">
			<div class="panel panel-success">
				<div class="panel-heading">
					<strong>
					<i class="fa fa-cart-plus fa-lg"></i>  Servicio de entrega directa de medicinas contrante Sidor, C.A
					</strong>
				</div>
				<div class="panel-body">
					<div class="table-responsive">
					<table class="table table-striped table-hover">
					
						<thead>
							<tr>
								<th>C.I Titular</th>
								<th>Nombre Titular</th>
								<th>Contratante</th>
								<th>C.I Beneficiario</th>
								<th>Nombre Beneficiario</th>
								<th>Farmacia</th>
								<th>Fecha</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><?php echo $datos_titular['tipo_doc']."-".$datos_titular['cedula']; ?></td>
								<td><?php echo $datos_titular['nombres']." ".$datos_titular['apellidos']; ?></td>
								<td><?php echo $datos_titular['nombre_contratante']; ?></td>
								<?php if($familiar_id == 0) { ?>
								<td><?php echo $datos_titular['tipo_doc']."-".$datos_titular['cedula']; ?></td>
								<td><?php echo $datos_titular['nombres']." ".$datos_titular['apellidos']; ?></td>
								<?php } else { ?>
								<td><?php echo $datos_familiar['tipo_doc']."-".$datos_familiar['cedula']; ?></td>
								<td><?php echo $datos_familiar['nombres']." ".$datos_familiar['apellidos']; ?></td>
								<?php } ?>
								<td><?php echo $name_farma['nombre']; ?></td>
								
								<td><?php echo date('d-m-Y / h:i. A') ?></td>
							</tr>
						</tbody>
					</table> 
					</div> <!-- table-responsive fin -->	
				</div>
			

<form action="php/registrar_facturas.php" method="POST" accept-charset="utf-8">
    <div class="row">
    	<div class="col-xs-12 col-md-12 table-responsive">
			<table class="table table-bordered table-hover table-sortable">
				<thead>
					<tr>
						<th class="text-center">
							Clave
						</th>
						<th class="text-center">
							Nro Factura
						</th>
						<th class="text-center" width="20%">
							Patologia
						</th>				
						<th class="text-center" width="35%">
							Medicamento despachado
						</th>
						<th class="text-center" width="5%">
							Cantidad
						</th>
    					<th class="text-center">
							Precio venta
						</th>
        				<th class="text-center" style="border-top: 1px solid #ffffff; border-right: 1px solid #ffffff;">
						</th>
					</tr>
				</thead>
				<tbody id="contenedor">
					<input type="hidden" name="titular" value="<?php echo $titular_id; ?>">
						<input type="hidden" name="familiar" value="<?php echo $familiar_id; ?>">
						<input type="hidden" name="farmacia" value="<?php echo $farma_id; ?>">
						<input type="hidden" name="contratante" value="<?php echo $datos_titular['nombre_contratante']; ?>">
    				<tr>
    					<td>
    						<input type="text" name="clave" class="form-control" placeholder="clave">
    					</td>
    					<td>
						    <input type="text" name="nro_factura"  placeholder="Nro factura" class="form-control" required pattern="^[0-9]{1,10}$" title="Solo se aceptan numeros en este campo" />
						</td>
    					<td>
    						<input type="text" name="patologia[]" class="form-control" placeholder="Patologia">
    					</td>						
						<td>
						    <input type="text" name="name_producto[]" placeholder="Nombre Medicamento" class="form-control" required/>
						</td>
						<td>								
							<input type="number" name="cantidad[]" id="cant" class="form-control text-center" value="1" min="1" required pattern="^[0-9]{1,10}$" title="Solo se aceptan numeros en este campo"/>							
						</td>
    					<td>
						    <input type="text" name="precio[]" placeholder="Incluir Iva" class="form-control" required/>
						</td>
                        <td>
                            <button type="button" name="del0" class="eliminar btn btn-danger glyphicon glyphicon-remove "></button>
                        </td>
					</tr>
					
					
				</tbody>
			</table>
		</div>
	<div class="padding pull-right">
		<button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i>  Guardar</button>
		<a id="add" class="btn btn-default"><i class="fa fa-plus"></i>  Añadir Otro Medicamento</a>
		
	</div>	
</div>
</form>
<?php if (isset($_GET['msg'])) {
    $msg= $_GET['msg']; ?>
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong><?php echo $msg; ?> </strong>
    </div>
<?php } ?>
</div></div></div></div>


<?php include("../includes/footer.php"); ?>
<script text="text/javascript">

$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $("#contenedor"); //Fields wrapper
    var add_button      = $("#add"); //Add button ID
    
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<tr><td></td><td></td><td><input type="text" name="patologia[]" class="form-control" placeholder="Patologia"></td><td><input type="text" name="name_producto[]" placeholder="Nombre Producto" class="form-control"/></td><td><input type="number" name="cantidad[]" id="cant" class="form-control text-center" value="1" min="1"/></td><td><input type="text" name="precio[]" placeholder="Incluir Iva" class="form-control"/></td><td><a name="del0" class="eliminar btn btn-danger glyphicon glyphicon-remove"></a></td></tr>'); //add input box
        }
    });
    
    $('tr').find("td button.eliminar").on("click", function() {
             $(this).closest("tr").remove();
             //$(this).parent("tr").remove(); x--;
        });

    // $("body").on("click",".eliminar", function(e){ //user click on remove text
    //     e.preventDefault(); $(this).parent("tr").remove(); x--;
    // })
});



</script>