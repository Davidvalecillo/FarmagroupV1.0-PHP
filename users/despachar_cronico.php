<?php 
	include("../includes/header.php");
	date_default_timezone_set('America/Caracas');
	include("../php/functions.php");
	$id = $_GET['id'];
	$farma_id = $_SESSION["farmacia"];
	$sql = mysql_query("SELECT * FROM datos_extras WHERE id = '{$id}' LIMIT 1");
?>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-success">
				<div class="panel-heading">
					<strong>
						<i class="fa fa-cart-plus fa-lg"></i>  Servicio de entrega directa de medicamentos
					</strong>
				</div>
				<div class="panel-body">
					<table class="table "  id="">
 						<thead>
 							<tr>
 								<th>C.I Titular</th>
								<th>Nombre Titular</th>
								<th>Contratante</th>
								<th>C.I Beneficiario</th>
								<th>Nombre Beneficiario</th>
								<th>Patologias</th>
								<th>Frecuencia</th>
								<th>Fecha</th>
 							</tr>
 						</thead>
 						<tbody>
 							<?php 
 								$data = mysql_fetch_assoc($sql); ?>

 								<tr class="">
 									<?php 
 									if ($data['tipo'] == "titular") {
 										$titular = mysql_query("SELECT * FROM datos_titular WHERE id = '{$data['beneficiario_id']}' LIMIT 1 ");
 										$tit = mysql_fetch_assoc($titular);?>

 											<td> <?php echo $tit['tipo_doc']."-".$tit['cedula']; ?> </td>
 											<td> <?php echo $tit['nombres']." ".$tit['apellidos']; ?> </td>
 											<td><?php echo $tit['nombre_contratante']; ?></td>
 											<td> <?php echo $tit['tipo_doc']."-".$tit['cedula']; ?> </td>
 											<td> <?php echo $tit['nombres']." ".$tit['apellidos']; ?> </td>
 																		
 											
 								<?php	}
 									else if($data['tipo'] == "familiar"){
 										$familiar = mysql_query("SELECT * FROM datos_familiar WHERE id = '{$data['beneficiario_id']}' LIMIT 1 ");
 										$fam = mysql_fetch_assoc($familiar);

 										$titu = mysql_query("SELECT * FROM datos_titular WHERE id = '{$fam['titular_id']}' LIMIT 1 ");
 										$titulares = mysql_fetch_assoc($titu);
 									?>
 										<td> <?php echo $titulares['tipo_doc']."-".$titulares['cedula']; ?> </td>
 										<td> <?php echo $titulares['nombres']." ".$titulares['apellidos']; ?> </td>
 										<td><?php echo $titulares['nombre_contratante']; ?></td>
 										<td> <?php echo $fam['tipo_doc']."-".$fam['cedula']; ?> </td>
 										<td> <?php echo $fam['nombres']." ".$fam['apellidos']; ?> </td>
 										
 								<?php	}  ?>

 										<td> 
 											<?php $pats = explode(",", $data['patologias']);
 													for ($i = 0; $i < count($pats); $i++) {
 														echo "<strong>". $pats[$i] ."</strong><br>";
 													}
 											?> 
 										</td>
 										<td> <?php echo $data['frecuencia_tratamiento']; ?> </td>
 										<td><?php echo date('d-m-Y / h:i. A') ?></td>
 																			
 								</tr> 								
 						</tbody>

 					</table>
 					<?php if($data['observacion'] != ""){ ?>
						<div class="alert alert-warning">
					        <i class="fa fa-exclamation-circle"></i>  <strong> OBSERVACION:</strong> <br>
					        <li><?php echo $data['observacion']; ?></li>				        
					    </div>
				    <?php } ?>
 					<?php if (isset($_GET['msg'])) {
 					    $msg= $_GET['msg']; ?>
 					    <div class="alert alert-info">
 					        <button type="button" class="close" data-dismiss="alert">&times;</button>
 					        <strong><i class="fa fa-check"></i> <?php echo $msg; ?> </strong>
 					    </div>
 					<?php }
 						else if (isset($_GET['error'])) {
 					    $msg= $_GET['error']; ?>
 					    <div class="alert alert-info">
 					        <button type="button" class="close" data-dismiss="alert">&times;</button>
 					        <strong><i class="fa fa-exclamation-circle"></i> <?php echo $msg; ?> </strong>
 					    </div>
 					<?php } ?>

 					<form action="php/registrar_facturas_cronicos.php" method="POST" accept-charset="utf-8">
 					    
				    	
							<table class="table table-bordered table-hover table-sortable">
								<thead>
									<tr>
										<th class="text-center">
											Clave u Orden
										</th>
										<th class="text-center">
											Nro Factura
										</th>				
										<th class="text-center" width="50%">
											Medicamento despachado
										</th>
										<th class="text-center">
											Cantidad
										</th>
				    					<th class="text-center">
											Precio venta
										</th>
				        				<th class="text-center" style="border-top: 1px solid #ffffff; border-right: 1px solid #ffffff;">
										</th>
									</tr>
								</thead>
								<tbody id="contenedor">
										<input type="hidden" name="id_cronicos" value="<?php echo $data['id']; ?>">
										<input type="hidden" name="titular" value="<?php echo $titulares['id']; ?>">
										<input type="hidden" name="familiar" value="<?php echo $fam['id']; ?>">
										<input type="hidden" name="farmacia" value="<?php echo $farma_id; ?>">
										<input type="hidden" name="contratante" value="<?php echo $titulares['nombre_contratante']; ?>">
										<input type="hidden" name="patologia" value="<?php echo $data['patologias']; ?>">
				    				
				    				<?php 
				    					$medicamentos = explode(",", $data['medicamento']);
				    					for ($i=0; $i < count($medicamentos); $i++) { ?>
				    					<tr>
					    					<td>
						    					<?php if($i > 0) { ?>
						    						
						    					<?php } else { ?>
						    							<input type="text" name="clave" class="form-control" placeholder="clave">
						    					<?php	} ?>
					    					</td>
					    					<td>
					    						<?php if($i > 0) { ?>
					    							
					    						<?php } else { ?>
					    								<input type="text" name="nro_factura"  placeholder="Nro factura" class="form-control" required pattern="^[0-9]{1,10}$" title="Solo se aceptan numeros en este campo" />
					    						<?php	} ?>
											</td>						
											<td>
											    <input type="text" name="name_producto[]" value="<?php echo $medicamentos[$i]; ?>" class="form-control" required/>
											</td>
											<td>								
												<input type="number" name="cantidad[]" id="cant" class="form-control text-center" value="1" min="1" required pattern="^[0-9]{1,10}$" title="Solo se aceptan numeros en este campo"/>							
											</td>
											
					    					<td>
											    <input type="number" name="precio[]" placeholder="Incluya Iva" class="form-control" required/>
											</td>
					                        <td class="eliminar">
					                        	<?php if($i > 0) { ?>
					                        		<button type="button" name="del0" class=" btn btn-danger glyphicon glyphicon-remove "></button>
					                        	<?php } else { ?>
					                        			
					                        	<?php	} ?>
					                        </td>
					                    </tr>
				    				<?php	}
				    				?>			
									
									
									
								</tbody>
							</table>
						<div class="padding pull-right">
							<button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i>  Registrar</button>
							<br>
						</div>	
							
					</form>


					<h3> Medicamentos Entregados al beneficiario: </h3> <hr>
					<?php 
						$cronicos = mysql_query("SELECT * FROM entregas_cronicos WHERE id_datos_cronicos = '{$id}' ");
						if (mysql_num_rows($cronicos) >= 1) { ?>
						
						<table class="table">
								<thead>
									<tr>
										<th>Nro</th>
										<th>Farmacia</th>
										<th>Medicamento</th>
										<th>Cantidad</th>
										<th>Fecha Ultima Entrega</th>
										<th>Dias Transcurridos</th>
										<th>Entregas</th>
									</tr>
								</thead>
								<tbody>									
						<?php
							$nro = 1;
							while ($entrega = mysql_fetch_assoc($cronicos)) { ?>

								<tr>
									<td> <?php echo $nro; $nro++; ?> </td>
									<td>
										<?php $farmacia = mysql_query("SELECT nombre FROM farmacias WHERE id = '{$entrega['id_farmacia']}' LIMIT 1");
											  $fm = mysql_fetch_assoc($farmacia);
											  echo $fm['nombre'];
										?>
									</td>
									<td> <?php echo $entrega['medicamento']; ?></td>
									<td> <?php echo $entrega['cantidad']; ?></td>
									<td> <?php echo strftime('%d %b de %G a las %I:%M %P', strtotime($entrega['fecha'])); ?> </td>
									<td> <?php contarDias($entrega['fecha']); ?> </td>
									<td> <?php echo $entrega['entregas']; ?></td>
								</tr>
								
							<?php }
						}
						else
						{
							echo "<i>No tiene entregas registradas</i>";
						}
					?>
						</tbody>
					</table>
						</div>
 						
				</div>				
			</div>
		
	</div>
</div>

<?php include("../includes/footer.php"); ?>

<script type="text/javascript">
$(document).ready(function() {
   $(document).on("click",".eliminar",function(){
       
       var eliminar = confirm("Esta seguro que desea Quitar el medicamento de la factura.?");
     	if ( eliminar ) {
     		var parent = $(this).parent();
       		$(parent).remove();
       	}
    });
 });
</script>