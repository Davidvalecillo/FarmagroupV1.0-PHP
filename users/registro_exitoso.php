<?php 
	include("../includes/header.php");

	//rescatando lod id de titular y familiar
	$titular_id = $_GET['titular'];
	$familiar_id = $_GET['familiar'];
	$farma_id = $_SESSION["farmacia"];
	date_default_timezone_set('America/Caracas');
	$hoy = date('Y-m-d');
	//consulta a datos de titulares y familiares
	$registros = mysql_query("SELECT * FROM factura WHERE
		titular_id = '{$titular_id}' AND familiar_id = '{$familiar_id}' 
		AND farmacia_id = '{$farma_id}' AND created_at = '{$hoy}' ");
 ?>

<div class="container">
	<center>
		<?php if (isset($_GET['msg'])) {
		    $msg= $_GET['msg']; ?>
		    <h3><span class="label label-success font">
		        <strong><i class="fa fa-check"></i> <?php echo $msg; ?> </strong>
		    </span></h3>
		<?php } ?>
		
	</center>
	<br>
</div>

<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12">
			<div class="panel panel-success">
				<div class="panel-heading">
					<i class="fa fa-cart-plus fa-lg"></i>  Servicio de entrega directa de medicinas contrante Sidor, C.A
				</div>
				<div class="panel-body">
					<div class="table-responsive">
					<table class="table table-striped table-hover">
					
						<thead>
							<tr class="font-tr">
								<th>C.I Titular</th>
								<th>Nombre Titular</th>
								<th>Contratante</th>
								<th>C.I Beneficiario</th>
								<th>Nombre Beneficiario</th>
								<th>Farmacia</th>
								<th>Patologia</th>
								<th>Fecha Creación</th>
								<th>Nro Factura</th>
								<th>Producto Despachado</th>
								<th>Cant</th>
								<th>Precio U</th>
								<th>Precio Venta</th>
							</tr>
						</thead>
						<tbody>
							
								<?php 
								$count = 0;
								$total_factura = 0;      
								while($datos = mysql_fetch_assoc($registros)){ 
									
									$id_titu = $datos['titular_id'];
									$id_flia = $datos['familiar_id'];
									$id_far = $datos['farmacia_id'];

									$titular_sql = mysql_query("SELECT * FROM datos_titular WHERE id = '{$id_titu}' LIMIT 1 ");
									$data_titular = mysql_fetch_assoc($titular_sql);

									$flia_sql = mysql_query("SELECT * FROM datos_familiar WHERE id = '{$id_flia}' LIMIT 1 ");
									$data_flia = mysql_fetch_assoc($flia_sql);

									$farma_sql = mysql_query("SELECT * FROM farmacias WHERE id = '{$id_far}' LIMIT 1 ");
									$data_farma = mysql_fetch_assoc($farma_sql);
								?>
								<tr class="font-tr">
									<?php if ($count > 0){ ?>
										<td></td><td></td>
										<td></td><td></td><td></td>
									<?php } else { ?>
									
										<td> <?php echo $data_titular['tipo_doc']."-".$data_titular["cedula"]; ?> </td>
										<td> <?php echo $data_titular['nombres']." ".$data_titular['apellidos']; ?> </td>
										<td> <?php echo $data_titular['nombre_contratante']; ?></td>
										<?php if($familiar_id == 0) {  ?>
											<td><?php echo $data_titular['tipo_doc']."-".$data_titular["cedula"]; ?></td>
											<td><?php echo $data_titular['nombres']." ".$data_titular['apellidos']; ?></td>
										<?php } else { ?>
											<td><?php echo $data_flia['tipo_doc']."-".$data_flia['cedula']; ?></td>
											<td><?php echo $data_flia['nombres']." ".$data_flia['apellidos']; ?></td>
										<?php } ?>
									<?php } ?>
									<td><?php echo $data_farma['nombre']; ?></td>
									<td> <?php echo $datos['patologia']; ?> </td>
									<td><?php echo $datos['created_at']; ?></td>
									<td><?php echo $datos['nro_factura']; ?></td>
									<td><?php echo $datos['nombre_producto']; ?></td>
									<td class="text-center"><?php echo $datos['cantidad']; ?></td>
									<td><?php echo $datos['precio_venta']; ?> Bs</td>
									<td><?php echo $datos['precio_total']; ?> Bs</td>
								</tr>
								<?php 
									$total_factura = $total_factura + $datos['precio_total'];
									$count++; 
								?>
								<?php } ?>
									<tr class="">
										<td colspan="9" rowspan="" headers=""></td>
										<td colspan="3" class="text-right">
											<strong>
												Total Facturado:												
											</strong>
										</td>
										<td colspan="1" rowspan="" headers="">
											<strong><?php echo $total_factura; ?> Bs </strong>
										</td>
									</tr>
						</tbody>
					</table> 
					</div> <!-- table-responsive fin -->	
				</div>

				</div>
			</div>
	</div>
	<center>
		<a href="factura.php?titular=<?php echo $titular_id;?>&familiar=<?php echo $familiar_id;?>" class="btn btn-link"><i class="fa fa-chevron-left"></i> Cargar más medicamentos al mismo beneficiario </a>
	
		<a href="index.php" class="btn btn-link">Ir a la búsqueda rapida <i class="fa fa-chevron-right"></i></a>
		
	</center>

</div>


 <?php include("../includes/footer.php"); ?>