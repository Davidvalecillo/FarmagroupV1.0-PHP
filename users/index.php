<?php 
	include("../includes/header.php");
	$farma_id = $_SESSION["farmacia"];
	$farma = mysql_query("SELECT * FROM farmacias WHERE id = '{$farma_id}' LIMIT 1");
	$name_farma = mysql_fetch_assoc($farma);
?>

<div class="container">
	<div class="row">
		<h1 class="font-farma"> Bienvenido usuario: <?php echo $name_farma['nombre']; ?>  </h1>
		<hr>
    <div class="col-xs-12 col-md-12 col-lg-12"> 
          <div class="panel panel-success ">
              <div class="panel-heading">
                  <i class="fa fa-search"></i> <strong> Busqueda rapida de beneficiarios titulares. </strong>  
              </div>
              <div class="panel-body">
                <center>
                <p>
                  <i class="fa fa-info-circle"></i> Ingrese la cedula de identidad de un beneficiario titular.</p>
                  <form class="form-inline" action="php/buscar.php" method="POST">
                    <div class="form-group">
                        <label class="sr-only" for="tipo_doc">tipo</label>
                        <select class="form-control" id="tipo_doc" name="tipo_doc">
                            <option>V</option>
                            <option>E</option>
                            <option>M</option>
                        </select>
                        <input type="text" class="form-control" name="cedula" id="cedula" placeholder="Cedula">
                    </div>
                    
                    <div class="form-group input-group">
                      <select class="form-control" name="contratante" required>
                          <option value="" selected>Contratante</option>
                          <option value="SIDOR, C.A.">Sidor</option>
                          <option value="MINISTERIO DEL PODER POPULAR PARA LA AGRICULTURA Y TIERRAS">Ministerio Agricultura y Tierras</option>
                      </select>
                      <span class="input-group-btn">
                          <button class="btn btn-success" type="submit"><i class="fa fa-search"></i> Buscar
                          </button>
                      </span>
                    </div>
                    <!-- <button type="submit" class="btn btn-success">Buscar</button> -->
                    <br> <br>
                    <?php if (isset($_GET['msg'])) {
                        $msg= $_GET['msg']; ?>
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong> <?php echo $msg; ?> </strong>
                        </div>
                    <?php } ?>
                  </form>  
                  </center>               
              </div> <!-- panel body -->
          </div><!--  panel -->
        </div> <!-- column -->
</div>
</div>

<div class="container">
  <div class="jumbotron">
    <p class="text-justify">
      <strong>Nota: </strong>  Los  medicamentos indicados deben estar cónsonos con las normas 
      médicas y farmacológicas aceptadas, reconocidos por la Federación Médica Venezolana y que 
      no sean de naturaleza experimental o de investigación, así mismo no debe entregar anticonceptivos.
    </p>

    <p><strong>Medicamentos que se encuentran cubiertos por la póliza:</strong></p>
    <ol>
      <li>Sólo para los hijos entre cero (0) y cinco (5) años vacunas, hasta Bs. 3.000,00 exceptuando 
          todos aquellos que otorga el Ministerio del Poder Popular para la Salud.
      </li>
      <li>
        Desensiblizantes para las alergias y vacunas para este tipo de tratamiento.
      </li>
      <li>
        Sondas, pañales, bolsas recolectoras de orina, bolsas de colostomía, 
        kit para pacientes insulino dependiente. <strong>(cobertura para personas con discapacidad).</strong>
      </li>
      <li>
        Cojín anti escara <strong>(uno por año)</strong>.
      </li>
      <li>Medicinas para el control de embarazo.</li>
    </ol>

    <p><strong>Listado de medicamentos no amparados por la póliza:</strong></p>
        <ol>
          <li>Tratamientos para la fertilidad.</li>
          <li>Polivitamínicos, multivitamínicos.</li>
          <li>Tratamientos de casas naturistas (FDC,NOW, NATURAL SISTEMS) entre otros.</li>
          <li>Espermicidas.</li>
          <li>Protectores solares.</li>
          <li>Tratamiento dermatológico con fines cosméticos.</li>
          <li>Vacunas.(Que sean suministradas por el IVSS).</li>
        </ol>
  </div>
</div>

<?php include("../includes/footer.php"); ?>