<?php include("../includes/header.php"); ?>
<?php 
//rescatando lod id de titular y familiar
$titular_id = $_GET['titular'];
$familiar_id = $_GET['familiar'];

//consulta a datos de titulares y familiares
$sql_titular = mysql_query("SELECT * FROM datos_titular WHERE id = '{$titular_id}' LIMIT 1");
$datos_titular = mysql_fetch_assoc($sql_titular);

$sql_familiar = mysql_query("SELECT * FROM datos_familiar WHERE id = '{$familiar_id}' AND titular_id = '{$titular_id}' LIMIT 1");
$datos_familiar = mysql_fetch_assoc($sql_familiar);

//consulta del nombre de la farmacia
$farma_id = $_SESSION["farmacia"];
$farma = mysql_query("SELECT * FROM farmacias WHERE id = '{$farma_id}' LIMIT 1");
$name_farma = mysql_fetch_assoc($farma);
?>

<div class="container">
	<div class="row">
		<div class="col-xs-12 col-md-12">
			<div class="panel panel-success">
				<div class="panel-heading">
					<i class="fa fa-cart-plus fa-lg"></i>  Servicio de entrega directa de medicinas contrante Sidor, C.A
				</div>
				<div class="panel-body">
					<div class="table-responsive">
					<table class="table table-striped table-hover">
					
						<thead>
							<tr>
								<th>C.I Titular</th>
								<th>Nombre Titular</th>
								<th>Contratante</th>
								<th>C.I Beneficiario</th>
								<th>Nombre Beneficiario</th>
								<th>Farmacia</th>
								<th>Fecha</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><?php echo $datos_titular['tipo_doc']."-".$datos_titular['cedula']; ?></td>
								<td><?php echo $datos_titular['nombres']." ".$datos_titular['apellidos']; ?></td>
								<td><?php echo $datos_titular['nombre_contratante']; ?></td>
								<?php if($familiar_id == 0) { ?>
								<td><?php echo $datos_titular['tipo_doc']."-".$datos_titular['cedula']; ?></td>
								<td><?php echo $datos_titular['nombres']." ".$datos_titular['apellidos']; ?></td>
								<?php } else { ?>
								<td><?php echo $datos_familiar['tipo_doc']."-".$datos_familiar['cedula']; ?></td>
								<td><?php echo $datos_familiar['nombres']." ".$datos_familiar['apellidos']; ?></td>
								<?php } ?>
								<td><?php echo $name_farma['nombre']; ?></td>
								
								<td><?php echo date('d-m-Y / h:i. A') ?></td>
							</tr>
						</tbody>
					</table> 
					</div> <!-- table-responsive fin -->	
				</div>

</div>
		</div>
	</div>
</div> <!-- div container -->


<div class="container">
    <div class="row clearfix">
    	<div class="col-md-12 table-responsive">
			<table class="table table-bordered table-hover table-sortable" id="tab_logic">
				<thead>
					<tr >
						<th class="text-center">
							Nro factura
						</th>
						<th class="text-center">
							Nombre producto despachado
						</th>
						<th class="text-center">
							cantidad
						</th>
    					<th class="text-center">
							Precio
						</th>
        				<th class="text-center" style="border-top: 1px solid #ffffff; border-right: 1px solid #ffffff;">
						</th>
					</tr>
				</thead>
				<tbody>
				<form action="php/registrar_facturas.php" method="POST" accept-charset="utf-8">
					<input type="hidden" name="titular" value="<?php echo $titular_id; ?>">
					<input type="hidden" name="familiar" value="<?php echo $familiar_id; ?>">
					<input type="hidden" name="farmacia" value="<?php echo $farma_id; ?>">


    				<tr id='addr0' data-id="0" class="">
						<td data-name="nro_factura">
						    <input type="text" name='nro_factura'  placeholder='Nro Factura' class="form-control"/>
						</td>
						<td data-name="name_producto">
						    <input type="text" name='name_producto' placeholder='Nombre Producto' class="form-control"/>
						</td>
						<td data-name="cantidad">
						    <input type="number" name="cantidad" value="1" min="1" max="15" class="form-control"/>
						</td>
    					<td data-name="precio">
						    <input type="text" name='precio' placeholder='Nombre Producto' class="form-control"/>
						</td>
                        <td data-name="del">
                            <button nam"del0" class='btn btn-danger glyphicon glyphicon-remove row-remove'></button>
                        </td>
					</tr>

				</tbody>
			</table>
		</div>
	</div>
	<div class="pull-right">
		<a id="add_row" class="btn btn-default">Add Row</a>
		<button type="submit" class="btn btn-success">Registrar</button>
	</div>
	
	</form>
</div>


<?php include("../includes/footer.php"); ?>
<script text="text/javascript">
$(document).ready(function() {
    $("#add_row").on("click", function() {
        // Dynamic Rows Code
        
        // Get max row id and set new id
        var newid = 0;
        $.each($("#tab_logic tr"), function() {
            if (parseInt($(this).data("id")) > newid) {
                newid = parseInt($(this).data("id"));
            }
        });
        newid++;
        
        var tr = $("<tr></tr>", {
            id: "addr"+newid,
            "data-id": newid
        });
        
        // loop through each td and create new elements with name of newid
        $.each($("#tab_logic tbody tr:nth(0) td"), function() {
            var cur_td = $(this);
            
            var children = cur_td.children();
            
            // add new td and element if it has a nane
            if ($(this).data("name") != undefined) {
                var td = $("<td></td>", {
                    "data-name": $(cur_td).data("name")
                });
                
                var c = $(cur_td).find($(children[0]).prop('tagName')).clone().val("");
                c.attr("name", $(cur_td).data("name") + '[]');
                c.appendTo($(td));
                td.appendTo($(tr));
            } else {
                var td = $("<td></td>", {
                    'text': $('#tab_logic tr').length
                }).appendTo($(tr));
            }
        });
        
        // add delete button and td
        /*
        $("<td></td>").append(
            $("<button class='btn btn-danger glyphicon glyphicon-remove row-remove'></button>")
                .click(function() {
                    $(this).closest("tr").remove();
                })
        ).appendTo($(tr));
        */
        
        // add the new row
        $(tr).appendTo($('#tab_logic'));
        
        $(tr).find("td button.row-remove").on("click", function() {
             $(this).closest("tr").remove();
        });
});




    // Sortable Code
    var fixHelperModified = function(e, tr) {
        var $originals = tr.children();
        var $helper = tr.clone();
    
        $helper.children().each(function(index) {
            $(this).width($originals.eq(index).width())
        });
        
        return $helper;
    };
  
    $(".table-sortable tbody").sortable({
        helper: fixHelperModified      
    }).disableSelection();

    $(".table-sortable thead").disableSelection();



    $("#add_row").trigger("click");
});
</script>