<?php 
	include("../includes/header.php");
	$consulta = mysql_query("SELECT * FROM usuarios WHERE id = '{$_SESSION['id']}' limit 1 ");
	$datos = mysql_fetch_assoc($consulta);
?>

 <div class="container">
 	<div class="row">
 		<h1>Mi Perfil: <?php echo $_SESSION['nombre']; ?> </h1> <hr>
 		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
 			<div class="panel panel-default">
 				<div class="panel-body">
 					<div class="">
 						<center>
 							<img src="../media/images/avatar.jpg" class="img-responsive img-circle" width="80px">
 							<hr>
 						</center>				
 					</div>
 					<h3>Actualizar Datos Personales:</h3>
 					<form action="php/editar_perfil.php" method="POST">
	 					<div class="form-group">
	 						<label class="control-label">Nombre:</label>
	 						<input type="text" name="nombre" class="form-control" value="<?php echo $datos['nombre']; ?>">
	 					</div>
	 					<div class="form-group">
	 						<label class="control-label">Cedula de Identidad:</label>
	 						<input type="number" name="cedula" class="form-control" value="<?php echo $datos['cedula']; ?>">
	 					</div>
	 					<button class="btn btn-success " type="submit" name="datos">Guardar Cambios</button>
 					</form>
 					<center>
 						<h4>
 							<?php if (isset($_GET['msg'])) {
 							    if ($_GET['msg'] == 'ok') { ?>
 							     	<span class="label label-success">
 							     		<i class="fa fa-check"></i>
 							     		Sus datos fueron actualizados con exito.
 							     	</span>	
 							<?php } elseif ($_GET['msg'] == 'not') { ?>
 									<span class="label label-danger">
 							     		<i class="fa fa-exclamation-triangle"></i>
 							     		Lo sentimos, ocurrio un error al actualizar.
 							     	</span>
 							<?php }					    	
 							 } ?>
 						</h4>
 					</center>
 					<hr>

 					<h3>Actualizar Datos de Acceso:</h3>
 					<form action="php/editar_perfil.php" method="POST">
	 					<div class="form-group">
	 						<label class="control-label">Email:</label>
	 						<input type="email" name="email" class="form-control" value="<?php echo $datos['email']; ?>" readonly>
	 					</div>
	 					<div class="form-group">
	 						<label class="control-label">Contraseña Actual:</label>
	 						<input type="password" name="clave_actual" class="form-control" value="">
	 					</div>

	 					<div class="form-group">
	 						<label class="control-label">Nueva contraseña:</label>
	 						<input type="password" name="clave_nueva" class="form-control" value="">
	 					</div>	

	 					<center>
	 						<h4>
	 							<?php if (isset($_GET['pass'])) {
	 							    if ($_GET['pass'] == 'ok') { ?>
	 							     	<span class="label label-success">
	 							     		<i class="fa fa-check"></i>
	 							     		Su contraseña fue actualizada con exito.
	 							     	</span>	
	 							<?php } elseif ($_GET['pass'] == 'not') { ?>
	 									<span class="label label-danger">
	 							     		<i class="fa fa-exclamation-triangle"></i>
	 							     		Lo sentimos, ocurrio un error al actualizar.
	 							     	</span>
	 							<?php }	
	 								elseif ($_GET['pass'] == 'err') { ?>
	 									<span class="label label-danger">
	 							     		<i class="fa fa-exclamation-triangle"></i>
	 							     		Lo sentimos, su clave actual no coincide.
	 							     	</span>
	 							<?php }							    	
	 							 } ?>
	 						</h4>
	 					</center>	
 				</div>

 				<div class="panel-footer">
 					<button class="btn btn-success" type="submit" name="acceso">Guardar Cambios</button>
 					</form>
 				</div>
 			</div>
 		</div>

 	</div>
 </div>


 <?php include("../includes/footer.php"); ?>